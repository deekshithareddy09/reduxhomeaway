import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './ProfileUser.css';
import axios from 'axios';
import moment from 'moment';
//import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
import DashboardHeader from '../Dashboard/DashboardHeader';
import DatePicker from 'react-datepicker';
import {Redirect} from 'react-router';
import { connect } from 'react-redux';
import { profile } from "../../actions";



class ProfileUser extends React.Component {
    constructor(props)
    {
        super(props);
       console.log("props",this.props);
         this.state={
             switch: false,
             firstname: null,
             lastname: null,
             username: null,
             aboutme: null,
             city: null,
             company: null,
             school: null,
             hometown: null,
             languages: null,
             gender: null,
             phone: null
             
         }
          this.handlefirstname = this.handlefirstname.bind(this);
          this.handlelastname = this.handlelastname.bind(this);
          this.handleaboutme = this.handleaboutme.bind(this);
          this.handlehomeinput=this.handlehomeinput.bind(this);
          this.handlecity= this.handlecity.bind(this);
          this.handlecompany=this.handlecompany.bind(this);
          this.handlegender=this.handlegender.bind(this);
          this.handlelanguages=this.handlelanguages.bind(this);
          this.handlephone=this.handlephone.bind(this);
          this.handleschool=this.handleschool.bind(this);
          this.handlesaveprofile=this.handlesaveprofile.bind(this);
 
    }
    componentDidMount() {
        //this.setState({lastname: this.props.signupdetails.signupdetails.lastname});
        //this.setState({username: this.props.signupdetails.signupdetails.username});
        this.setState({firstname: this.state.firstname});
        this.setState({lastname: this.state.lastname});
        this.setState({username: this.state.username});
        this.setState({aboutme: this.state.aboutme});
        this.setState({homeinput: this.state.homeinput});
        this.setState({city: this.state.city});
        this.setState({company: this.state.company});
        this.setState({gender: this.state.gender});
        this.setState({languages: this.state.languages});
        this.setState({phone: this.state.phone});
        //this.setState({firstname: this.props.location.state.firstname});
        //this.setState({firstname: this.props.location.state.firstname});


     }
     

    // componentWillMount(){
    //     //e.preventDefault();
    //     const signupdata = {
    //         firstname : this.props.location.state.firstname,
    //         lastname : this.props.location.state.lastname
    //     }
    //     axios.get('/profileuserdetails')
    //     .then(response => {
    //         console.log("Status Code : ",response.status);
    //         if(response.status === 200){
    //             console.log("response data "+response.data);
    //             this.setState({
    //                 switch: true
    //             }
    //             )
    //         }else{
    //             console.log("error in getting the response ");
    //         }
    //     });


    //}
    
    handlefirstname = (e) => {
        this.setState({
             firstname : e.target.value
         })
       }
       
    handlelastname = (e) => {
        this.setState({
             lastname : e.target.value
         })
       }
       
    handleaboutme = (e) => {
        this.setState({
             aboutme : e.target.value
         })
       }
    handlecity = (e) => {
        this.setState({
             city : e.target.value
         })
       }
       handlelanguages = (e) => {
        this.setState({
             languages : e.target.value
         })
       }
       handlecompany = (e) => {
        this.setState({
             company : e.target.value
         })
       }
       handlegender = (e) => {
        this.setState({
             gender : e.target.value
         })
       }
       handleschool = (e) => {
        this.setState({
             school : e.target.value
         })
       }
       handlephone = (e) => {
        this.setState({
             phone : e.target.value
         })
       }
      handlehomeinput = (e) => {
        this.setState({
             hometown: e.target.value
         })
       }
       handlesaveprofile = (e) => {
        e.preventDefault();
        const profiledata = {
            firstname : this.state.firstname,
            lastname : this.state.lastname,
            aboutme: this.state.aboutme,
            city: this.state.city,
            company: this.state.company,
            school: this.state.school,
            hometown: this.state.hometown,
            languages: this.state.languages,
            gender: this.state.gender,
             phone: this.state.phone,
             username: localStorage.username
        }
        this.props.profile(profiledata);
        }
     render() {   
         console.log("local username"+localStorage.username); 
        return(
            <div>
            <DashboardHeader/>
            <div class="section-with-border no-bottom-padding">
        <div class="row">
            <div class="col-xs-8 hidden-xs">
                <h3 class="section-header">
                    Profile information
                </h3>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <a class="facebook-import-link js-facebook-import">
                    Import
                    <div class="social-icon img-circle text-center">
                        <i class="icon-facebook icon-white"></i>
                    </div>
                </a>
            </div>
        </div>

            <div class="row form-group ">
                <label class="col-xs-12 sr-only" for="profileFirstNameInput">First name</label>
                <div class="col-sm-12 col-md-7"  >
                    <input type="text"  class="form-control input-lg js-input-field"  onChange={this.handlefirstname} id="profileFirstNameInput" value={this.state.firstname} placeholder="First name"  data-input-model-name="firstName" maxlength="100" required=""/>
                </div>
            </div>
            <div class="row form-group ">
                <label class="col-xs-12 sr-only" for="profileLastNameInput">Last name or initial</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control input-lg js-input-field" id="profileLastNameInput" onChange={this.handlelastname}  value={this.state.lastname} placeholder="Last name or initial"  data-input-model-name="lastName" maxlength="100" required=""/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileAboutInput">About me</label>
                <div class="col-xs-12">
                    <textarea type="text" class="form-control input-lg js-input-field" rows="4" onChange={this.handleaboutme} id="profileAboutInput"  value={this.state.aboutme} placeholder="About me" data-input-model-name="about" maxlength="4000"></textarea>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileCityInput">Current City</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text"  value={this.state.city} class="form-control input-lg js-input-field"  onChange={this.handlecity} id="profileCityInput" placeholder="My city, country"   data-input-model-name="currentCity" maxlength="80"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileCompanyInput">Company</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text" value={this.state.company} class="form-control input-lg js-input-field" id="profileCompanyInput"  onChange={this.handlecompany} placeholder="Company"   data-input-model-name="company" maxlength="100"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileSchoolInput">School</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text" value={this.state.school}  class="form-control input-lg js-input-field" id="profileSchoolInput" onChange={this.handleschool} placeholder="School"   data-input-model-name="school" maxlength="100"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileHometownInput">Hometown</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text"  value={this.state.hometown} class="form-control input-lg js-input-field" id="profileHometownInput" onChange={this.handlehomeinput} placeholder="Hometown"   data-input-model-name="hometown" maxlength="80"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileLanguageInput">Languages</label>
                <div class="col-sm-12 col-md-7">
                    <input type="text" value={this.state.languages} class="form-control input-lg js-input-field" onChange={this.handlelanguages} id="profileLanguageInput" placeholder="Languages"   data-input-model-name="languages" maxlength="100"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-12 sr-only" for="profileGenderInput">Gender</label>
                <div class="col-sm-12 col-md-7">
                    <select onChage={this.handlegender} value={this.state.gender} class="form-control input-lg js-input-field" id="profileGenderInput" data-input-model-name="gender">
                            <option selected="selected">Gender</option>
                            <option value="female">Female</option>
                            <option value="male">Male</option>
                            <option value="other">Other</option>
                    </select>
                </div>
                <span class="col-xs-12 help-block">
                    <svg class="inline-svg svg-brand"></svg>
                    This is never shared
                </span>
            </div>
            
            <div class="row form-group" id="mediated-sms-preference">
                <div class="pull-left switch">
                    <input type="checkbox" name="settingsSmsPreference" id="settingsSmsPreference" class="js-settings-toggle-sms-preference" aria-checked=""/>
                    <label for="settingsSmsPreference">
                        <svg class="inline-svg switch-checked">
                            
                        </svg>
                        <div class="switch-toggle"></div>
                    </label>
                </div>
                <div class="col-md-7 pull-left">
                    <span class="sms-pref-title">Send me texts about my bookings</span>
                    <span class="sms-pref-info">Only available for mobile phones in select countries. Standard messaging rates apply. See <a href="https://www.homeaway.com/info/about-us/legal/terms-conditions" target="_blank">terms and conditions</a> and <a href="https://www.homeaway.com/info/about-us/legal/privacy-policy" target="_blank">privacy policy.</a></span>
                </div>
            </div>
            
                <div class="row form-group">
                    <label class="col-xs-12 sr-only" for="phone0">Phone number</label>
                    <div class="col-sm-12 col-md-7">
                        <section data-phone-index="0" class="phone" id="phone-container-0">
                        <div class="input-group phone-number-input js-phoneInputContainer">
            <div class="input-group-btn selected-flag-container">
                <button class="btn dropdown-toggle btn-default js-btn-country-flag selected-flag" data-toggle="dropdown" data-effect="ripple" aria-haspopup="true" aria-expanded="false">
                    <span class="js-countryPhoneCode-sr-only sr-only">United States - Country calling code: +1</span>
                    <i class="js-selectedFlag flag-us" aria-hidden="true"></i>
                    <i class="icon-chevron-down" aria-hidden="true"></i>
                </button>
            </div>
        <div class="form-group has-icon">
            <span class="form-control-icon js-countryPhoneCode form-control-icon-country-phone-code" aria-hidden="true">+1</span>
            <label for="phone" class="sr-only">Phone Number</label>
            <input type="tel"  value={this.state.phone} name="phone" onChange={this.handlephone}  data-prop="phone" data-countrycode="us" class="js-phone form-control cc-length-1"/>
        </div>
</div>
</section>
                    </div>

                    <span class="col-xs-12 help-block repeat-number hidden">
                        You've already entered this number. Please enter a different number.
                    </span>

                    <span class="col-xs-12 help-block add-another-phone-section">
                        <a id="add-another-phone-link">Add another phone number</a>
                    </span>
                </div>
                <div class="row form-group hidden">
                    <label class="col-xs-12 sr-only" for="phone1">Phone number</label>
                    <div class="col-sm-12 col-md-7">
                        <section data-phone-index="1" class="phone" id="phone-container-1">
                        <div class="input-group phone-number-input js-phoneInputContainer">
            <div class="input-group-btn selected-flag-container">
                <button class="btn dropdown-toggle btn-default js-btn-country-flag selected-flag" data-toggle="dropdown" data-effect="ripple" aria-haspopup="true" aria-expanded="false">
                    <span class="js-countryPhoneCode-sr-only sr-only">United States - Country calling code: +1</span>
                    <i class="js-selectedFlag flag-us" aria-hidden="true"></i>
                    <i class="icon-chevron-down" aria-hidden="true"></i>
                </button>
              
            </div>
        <div class="form-group has-icon">
            <span class="form-control-icon js-countryPhoneCode form-control-icon-country-phone-code" aria-hidden="true">+1</span>
            <label for="phone" class="sr-only">Phone Number</label>
            <input type="tel" name="phone"  onChange={this.handlephone} data-prop="phone" data-countrycode="us" class="js-phone form-control cc-length-1"/>
        </div>
<span class="input-group-btn"><button class="btn btn-default remove-phone-btn" type="button"><i class="icon-close  icon-danger"></i></button></span></div>
</section>
                    </div>

                    <span class="col-xs-12 help-block repeat-number hidden">
                        You've already entered this number. Please enter a different number.
                    </span>

                    <span class="col-xs-12 help-block add-another-phone-section hidden">
                        <a id="add-another-phone-link">Add another phone number</a>
                    </span>
                </div>
                <div class="row form-group hidden">
                    <label class="col-xs-12 sr-only" for="phone2">Phone number</label>
                    <div class="col-sm-12 col-md-7">
                        <section data-phone-index="2" class="phone" id="phone-container-2">
                        <div class="input-group phone-number-input js-phoneInputContainer">
            <div class="input-group-btn selected-flag-container">
                <button class="btn dropdown-toggle btn-default js-btn-country-flag selected-flag" data-toggle="dropdown" data-effect="ripple" aria-haspopup="true" aria-expanded="false">
                    <span class="js-countryPhoneCode-sr-only sr-only">United States - Country calling code: +1</span>
                    <i class="js-selectedFlag flag-us" aria-hidden="true"></i>
                    <i class="icon-chevron-down" aria-hidden="true"></i>
                </button>
                
            </div>
        <div class="form-group has-icon">
            <span class="form-control-icon js-countryPhoneCode form-control-icon-country-phone-code" aria-hidden="true">+1</span>
            <label for="phone" class="sr-only">Phone Number</label>
            <input type="tel" name="phone"   data-prop="phone" data-countrycode="us" class="js-phone form-control cc-length-1"/>
        </div>
<span class="input-group-btn"><button class="btn btn-default remove-phone-btn" type="button"><i class="icon-close  icon-danger"></i></button></span></div>
</section>
                    </div>

                    <span class="col-xs-12 help-block repeat-number hidden">
                        You've already entered this number. Please enter a different number.
                    </span>

                    <span class="col-xs-12 help-block add-another-phone-section hidden">
                        <a id="add-another-phone-link">Add another phone number</a>
                    </span>
                </div>
                <div class="row form-group hidden">
                    <label class="col-xs-12 sr-only" for="phone3">Phone number</label>
                    <div class="col-sm-12 col-md-7">
                        <section data-phone-index="3" class="phone" id="phone-container-3">
                        <div class="input-group phone-number-input js-phoneInputContainer">
            <div class="input-group-btn selected-flag-container">
                <button class="btn dropdown-toggle btn-default js-btn-country-flag selected-flag" data-toggle="dropdown" data-effect="ripple" aria-haspopup="true" aria-expanded="false">
                    <span class="js-countryPhoneCode-sr-only sr-only">United States - Country calling code: +1</span>
                    <i class="js-selectedFlag flag-us" aria-hidden="true"></i>
                    <i class="icon-chevron-down" aria-hidden="true"></i>
                </button>
                
            </div>
        <div class="form-group has-icon">
            <span class="form-control-icon js-countryPhoneCode form-control-icon-country-phone-code" aria-hidden="true">+1</span>
            <label for="phone" class="sr-only">Phone Number</label>
            <input type="tel" name="phone"   data-prop="phone" data-countrycode="us" class="js-phone form-control cc-length-1"/>
        </div>
<span class="input-group-btn"><button class="btn btn-default remove-phone-btn" type="button">
<i class="icon-close  icon-danger"></i></button></span></div>
</section>
                    </div>

                    <span class="col-xs-12 help-block repeat-number hidden">
                        You've already entered this number. Please enter a different number.
                    </span>

                    <span class="col-xs-12 help-block add-another-phone-section hidden">
                        <a id="add-another-phone-link">Add another phone number</a>
                    </span>
                </div>
            </div>
            <div class="profile-form-footer">
        <button type="button" class="btn btn-primary" onClick={this.handlesaveprofile} data-loading-text="Sending...">Save changes</button>
    </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { signupdetails: state.signupdetails };
  }
        
export default connect(mapStateToProps,{ profile })(ProfileUser);

