
import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import axios from 'axios';
import './OwnerDashboard.css';
import {Redirect} from 'react-router';
import CCtest from '../CCtest/CCtest';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
class TravelerBookings extends React.Component {
    constructor(props){
    super(props);
    this.state = {
        propertydata: [],
        switch: false
    }
}
componentDidMount(){
     console.log("cd   "+localStorage.owneremailid);
     const ownerusername = {
         username : localStorage.username
     }
     console.log("in dashboard username"+ownerusername.username);
    axios.post('/travelerbookings',ownerusername)
            .then((response) => {
                console.log(response.data);
            this.setState({
                bookingdata : this.state.bookingdata.concat(response.data) 
            });
            console.log(this.state.bookingdata);
        });
}
render() {    
    if(this.state.switch){
     return <Redirect to={{pathname: '/Tripboardafterclick',state: {emailid: this.props.emailid}
  }} />;}

  let details = this.state.propertydata.map(propertydataitem => {
    console.log("propid"+propertydataitem.propid); 
    return(
        <div> 
        <table class=" col-xs-6 col-sm-8 list-table listingTable table table-hover">
        <div className="carouselowner">
             <CCtest propertyid= {propertydataitem.propid} /> 
            </div>
   <tbody>
      <tr class="INCOMPLETE">
         <td class="listing-summary" data-listingguid="121.7352502.6707776" data-partnerfeedenabled="true">
            <div class="property-combine">
               <div class="alerts-summary">
                  <span data-automation-id="LISTING_INCOMPLETE" class="alert-inner text-warning">
                  </span>
               </div>
               <div class="row-fluid">
                  <div class="span2">
                     <a href="/pxe/feed/121.7352502.6707776" class="thumbnail-link">
                     </a>
                  </div>
                  <div class="span8">
                     <div class="property-details">
                        <h3>
                            Property  {propertydataitem.headline}
                        </h3>
                        <div class="address">
                        {propertydataitem.address+ propertydataitem.city + propertydataitem.state}<br/>
                        </div>
                        <span class="listing-ids">
                        {/* HomeAway.com {propertydataitem.id} */}
                        </span>
                        {/* <span class="inline-rating-container" id="inline-rating-container-listing-121_7352502_6707776" data-listingid="121.7352502.6707776">
                        <a href="/gd/dash/reviews/121.7352502.6707776"><span class="rating rating-0"></span></a></span> */}
                     </div>
                  </div>
                  <div class="buttons pull-right span2 buttons-dropdown-block">
                     <div class="btn-group property-option">
                        <a class="dropdown-toggle btn" role="button" data-toggle="dropdown" href="#"><span class="text">
                        Manage</span>&nbsp;<i class="icon-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" role="menu">
                           <li><a href="/pob/checklist/121.7352502.6707776">
                              Complete Listing
                              </a>
                           </li>
                           <li class="divider" role="presentation"></li>
                           <li>
                              <a href="#" class="archiveListing" data-listingref="121.7352502.6707776" data-sitename="HomeAway.com" data-listingnumber="7352502">
                              Archive HomeAway.com 
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </td>
      </tr>
         </tbody>
</table>
</div>
        
    )
})

  return (
   <div>
    <HeaderOwnerDashboard/>   
    <Link to="/TravelerBookings" className="btn btn-secondary">Click for Travelers Booked</Link>
    {details}
   </div>
            
    );
  }   
}
export default connect(null)(OwnerDashboard);
























