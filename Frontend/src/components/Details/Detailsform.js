import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';

class Detailsform extends Component {

  //Define component that you wanbt to render
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }



  
  /*Action call
  Whenever onSubmit event is triggered, execute an action call called createBook 
  */
  onSubmit(values) {
    console.log("values",values);
    this.props.createLocation(values, () => {
      this.props.history.push("/Bookingform");
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (

      //handleSubmit is the method that comes from redux and it tells redux what to do with the submitted form data
      //Field is a component of redux that does the wiring of inputs to the redux store.
      <div>
      <HeaderOwnerDashboard />
      <div class="col-xs-6 col-sm-8 content-panel-container">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div>
                        <div class="checklist-header-container ">
                           <h3><span>Verify your rental</span></h3>
                           <hr/>
                        </div>
                        <div>
                           <div></div>

     <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
      
        <Field
          label="Start out with a descriptive headline and a detailed summary of your property."
          name="headline"
          component={this.renderField}
        />
        <Field
          label="Property description"
          name="description"
          component={this.renderField}
        />
                <div><label>Propertytype</label></div>
         <Field label="Propertytype" 
                name="propertytype" 
                component="select">
                <option value="apartment">Apartment</option>
                <option value="barn">Barn</option>                            
                <option value="bed &amp; breakfast">Bed &amp; Breakfast</option>
                <option value="boat">Boat</option>
                <option value="bungalow">Bungalow</option>
                <option value="cabin">Cabin</option>
                <option value="campground">Campground</option>
                <option value="castle">Castle</option>
                <option value="chalet">Chalet</option>
                <option value="country house / chateau">Chateau / Country House</option>
                <option value="condo">Condo</option>
                <option value="corporate apartment">Corporate Apartment</option>
                <option value="cottage">Cottage</option>
                <option value="estate">Estate</option>
                <option value="farmhouse">Farmhouse</option>
                <option value="guest house/pension">Guest House</option>
                <option value="hostel">Hostel</option>
                <option value="hotel">Hotel</option>
                <option value="hotel suites">Hotel Suites</option>
                <option value="house">House</option>
                <option value="house boat">House Boat</option>
                <option value="lodge">Lodge</option>
                <option value="Mill">Mill</option>
                <option value="mobile home">Mobile Home</option>
                <option value="Recreational Vehicle">Recreational Vehicle</option>
                <option value="resort">Resort</option>
                <option value="studio">Studio</option>
                <option value="Tower">Tower</option>
                <option value="townhome">Townhome</option>
                <option value="villa">Villa</option>
                <option value="yacht">Yacht</option>                                           
          </Field>

        <Field
          label="Bedrooms"
          name="bedrooms"
          component={this.renderField}
        />
        <Field
          label="Bathrooms"
          name="bathrooms"
          component={this.renderField}
        />
        <Field
          label="Accomaodates"
          name="accomodates"
          component={this.renderField}
        />
         
         <button type="submit" className="btn btn-primary">Submit</button>
         <Link to="/Locationform" className="btn btn-primary">Back</Link>
      </form>
      </div>
                     </div>
                     <hr/>
                  </div>
               </div>
               </div>
      </div>
    );
  }
}

function validate(values) {

  const errors = {};

  // Validate the inputs from 'values'
  if (!values.headline) {
    errors.headline = "Enter an headline";
  }
  if (!values.description) {
    errors.description = "Enter description";
  }
  if (!values.bedrooms) {
    errors.bedrooms = "Enter bedrooms";
  }
  if (!values.bathrooms) {
    errors.bathrooms = "Enter bathrooms";
  }
  if (!values.accomodates) {
    errors.accomodates = "Enter accomodates";
  }




  // If errors is empty, the form is fine to submit
  // If errors has *any* properties, redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: "NewBookForm",
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(connect(null, { createLocation })(Detailsform));
