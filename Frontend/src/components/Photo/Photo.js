import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './Photo.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from './HeaderOwnerDashboard';
var newArray=[];
class Photo extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props);
        this.state = {
            id: 0,
            description: '',
            selectedFile: [],
            imageView : '',
            imgArray:[],
            count:0,
            switch: false
        }
        //tg44ethis.displayAccountForm = this.displayAccountForm.bind(this);
        this.handlenext = this.handlenext.bind(this);
    
    }
    onChange = (e) => {
   
        if(e.target.name == 'selectedFile'){
        
            
            
            for(var i=0;i<e.target.files.length;i++)
            {
    
                newArray.push(e.target.files[i]);
                 console.log(e.target.files[i])
           
        }
            console.log("new array is"+newArray);
            console.log("file selected is"+(e.target.files));
           
           
          }else{
            this.setState({ [e.target.name]: e.target.value });
            console.log("else in onchange"+this.state.description);
          }
      }
      handlenext = (e) => {
        e.preventDefault();
        this.setState({
            id: this.state.id + 1
        })
        const { description, selectedFile } = this.state;
        let formData = new FormData();
        console.log("e.target.files"+newArray.length);
        for(var i=0;i<newArray.length;i++)
        {
          formData.append('newArray', newArray[i]);
        }
        console.log(localStorage.propid);
        formData.append('photoid',localStorage.propid);
        // formData.append('selectedFile', selectedFile)
        // formData.append('newArray', newArray);
        for (var value of formData.values()) {
            console.log(value); 
        }         
        console.log(formData);
        axios.post('/photoupload',formData)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                console.log("response data ");
                this.setState({
                    switch: true
                }
                )
            }else{
                console.log("error in getting the response ");
            }
        });
      }
     render() {
        if(this.state.switch){
            return <Redirect to={{pathname: '/Availability'
            }} />;}        
        const {  selectedFile } = this.state;
        console.log(this.props);  
        return (
            <div>
            <HeaderOwnerDashboard />
            <div class="panel panel-default pyl-photos">
   <div>
      <div class="le-nav-header">
         <h1 class="nav-header-text">Add up to 5 photos of your property</h1>
      </div>
      <hr>
      </hr>
   </div>
   <div class="pyl-photos-container">
      <div class="pyl-photos-content">
         <div>
            <div class="pyl-photos-subheading">Showcase your property’s best features (no pets or people, please). Requirements: JPEG, at least 1920 x 1080 pixels, less than 20MB file size, 6 photos minimum. <a href="http://www.realtourvision.com/vacation-rental-photos.php" target="_blank">Need photos? Hire a professional.</a></div>
            <div class="pyl-photos-subheading xs">Showcase your property’s best features (no pets or people, please). Requirements: JPEG, at least 1920 x 1080 pixels, less than 20MB file size, 6 photos minimum. <a href="http://www.realtourvision.com/vacation-rental-photos.php" target="_blank">Need photos? Hire a professional.</a></div>
         </div>
         <div class="pyl-photos-photo-drop">
            <div id="inside-upload-area">
               <div class="pyl-photos-photo-drop-inside">
                  <h1 class="photo-drop-title text-muted">Drop photos here</h1>
                  <h1 class="photo-drop-OR text-muted">or</h1>
                  <h1 class="photo-drop-error text-muted">Only JPEG images are supported</h1>
                  <div><label for="uploadPhotoInput" class="photo-drop-button btn btn-default center-block">SELECT PHOTOS TO UPLOAD</label>
                  <input
                  type="file"
                  id="uploadPhotoInput"
                  name="selectedFile"
                  multiple 
                  onChange={this.onChange}
                  />                 
                  </div>
                 
                  <div class="photo-drop-info small text-muted">0 of 5 uploaded. 2 are required. You can choose to upload more than one photo at a time.</div>
               </div>
            </div>
         </div>
         <div class="pyl-photos-shot-list"></div>
         <div class="le-nav-footer">
            <div class="nav-footer-back">
            <Link to="/Bookingform" className="btn btn-primary">Back</Link>
            </div>
            <div>
               <div class="nav-footer-next">
               <div class="col-xs-8"><button class="btn btn-primary " type="button" onClick={this.handlenext}>Next</button></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

            
        );
    }
}

export default connect(null)(Photo);
