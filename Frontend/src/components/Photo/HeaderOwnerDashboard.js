import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './Photo.css';
import DropdownafterDashboard from '../Dropdown/DropdownafterDashboard';
import axios from 'axios';
import Dropdown from '../Dropdown/Dropdown';
class HeaderOwnerDashboard extends React.Component {
     render() {    
        return ( 
            <div class="on-checklist checklistclass">
            <div id="root">
            <div>
               <div class="ha-partner-nav-header-impersonation-hidden"></div>
               <div></div>
               <header class="ha-partner-nav-header  ha-partner-nav-header-loaded theme-gt generic " style={{'top': '0px'}}>
                  <a class="ha-partner-logo-section" href="/haod/"><img class="ha-partner-logo " src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.6/bce/moniker/homeaway_us/logo-bceheader.svg"/></a>
                  <div class="ha-partner-header-section ha-partner-nav-app-switcher ha-partner-nav-app-switcher-closed ">
                     <div class="ha-partner-nav-app-switcher-icon">
                        <div class="ha-partner-nav-app-switcher-icon-grid">
                           <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                              <g fill="currentColor">
                                 <rect x="1" y="1" width="4" height="4"></rect>
                                 <rect x="9" y="1" width="4" height="4"></rect>
                                 <rect x="17" y="1" width="4" height="4"></rect>
                                 <rect x="1" y="9" width="4" height="4"></rect>
                                 <rect x="9" y="9" width="4" height="4"></rect>
                                 <rect x="17" y="9" width="4" height="4"></rect>
                                 <rect x="1" y="17" width="4" height="4"></rect>
                                 <rect x="9" y="17" width="4" height="4"></rect>
                                 <rect x="17" y="17" width="4" height="4"></rect>
                              </g>
                           </svg>
                        </div>
                        <div class="ha-partner-nav-app-switcher-icon-close">
                           <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                              <path fill="currentColor" d="M2.2,0.8l17,17l-1.4,1.4l-17-17L2.2,0.8z M0.8,17.8l17-17l1.4,1.4l-17,17L0.8,17.8z"></path>
                           </svg>
                        </div>
                     </div>
                     <span class="ha-partner-nav-app-switcher-title">Resources</span>
                     
                  </div>
                  <div>
                     <div class="ha-partner-header-section ha-partner-nav-cart-empty"><a class="ha-partner-nav-cart"><i class="ha-partner-nav-cart-icon"></i></a></div>
                     <div class="ha-partner-notification-center-container ">
                        <svg width="22px" height="22px" viewBox="0 0 20 22" version="1.1" xmlns="http://www.w3.org/2000/svg">
                           <g stroke="#353E44" stroke-width="1.55" fill="none" fill-rule="evenodd" stroke-linecap="square" transform="translate(-1310.000000, -19.000000)">
                              <g transform="translate(1312.000000, 14.000000)">
                                 <g transform="translate(0.000000, 6.000000)">
                                    <path d="M10.9090909,17.2727273 C10.9090909,18.8181818 9.72727273,20 8.18181818,20 C6.63636364,20 5.45454545,18.8181818 5.45454545,17.2727273"></path>
                                    <path d="M14.5454545,10.9090909 C14.5454545,8.63636364 14.5454545,6.36363636 14.5454545,6.36363636 C14.5454545,2.81818182 11.7272727,0 8.18181818,0 C4.63636364,0 1.81818182,2.81818182 1.81818182,6.36363636 C1.81818182,6.36363636 1.81818182,8.63636364 1.81818182,10.9090909 C1.81818182,14.5454545 0,17.2727273 0,17.2727273 L16.3636364,17.2727273 C16.3636364,17.2727273 14.5454545,14.5454545 14.5454545,10.9090909 Z"></path>
                                 </g>
                              </g>
                           </g>
                        </svg>
                        <div class="ha-partner-notification-center-list-container collapsed">
                           <div class="notification-center-list-item-container">
                              <div class="notification-center-list-item-title"></div>
                              <div class="notification-center-list-item-notification-text">No new notifications.</div>
                           </div>
                        </div>
                     </div>
                     <div class="ha-partner-nav-account ha-partner-nav-account-collapsed ha-partner-header-section">
                        <div class="ha-partner-nav-account-header">
                           <svg class="ha-partner-default-user-avatar" width="81px" height="81px" viewBox="0 0 81 81" version="1.1" xmlns="http://www.w3.org/2000/svg">
                              <g id="MVP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                 <g transform="translate(-4.000000, -4.000000)">
                                    <g transform="translate(4.000000, 4.000000)">
                                       <g>
                                          <mask id="mask-2" fill="white">
                                             <circle cx="40.5" cy="40.5" r="40.5"></circle>
                                          </mask>
                                          <circle cx="40.5" cy="40.5" r="40.5" fill="#353E44"></circle>
                                          <path d="M48.776466,28.3505709 C48.9673306,24.1982998 45.331637,20.9508492 41.0825823,20.9508492 C36.8326011,20.9508492 33.145022,24.2262096 33.3886987,28.3505709 C33.4368781,29.1797646 34.242957,33.2834185 34.242957,33.2834185 C34.9082037,36.846881 37.3051301,39.861149 41.0825823,39.861149 C44.8591081,39.861149 47.2087816,36.8927973 47.9212812,33.2834185 C47.9212812,33.2834185 48.7384783,29.180665 48.776466,28.3505709" fill="#FFFFFF" opacity="0.824898098" mask="url(#mask-2)"></path>
                                          <path d="M57.5226562,54.4043077 C57.5226562,54.4043077 50.2762854,56.7685453 40.7302734,56.7685453 C31.1842615,56.7685453 23.9378906,54.4043077 23.9378906,54.4043077 L24.7439695,47.94722 C25.082152,45.8368723 26.8545989,44.5125031 28.7938208,44.0749481 L40.7302734,41.7971411 L52.6602404,44.0614433 C54.6328173,44.5422137 56.3774684,45.8071618 56.7156509,47.9364162 L57.5226562,54.4043077 Z" fill="#FFFFFF" opacity="0.824898098" mask="url(#mask-2)"></path>
                                       </g>
                                    </g>
                                 </g>
                              </g>
                           </svg>
                           {/* <div class="ha-partner-nav-account-title">My account</div> */}
                           <DropdownafterDashboard />
                           <span class="ha-partner-nav-account-chevron-icon ">
                              <svg width="12px" height="6px" viewBox="0 0 12 6" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                 <g stroke="currentColor" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-284.000000, -356.000000)" stroke="#353E44">
                                       <g>
                                          <g transform="translate(0.000000, 60.000000)">
                                             <path d="M292.498754,293.501246 L287.577666,298.313137 C287.47245,298.416288 287.47245,298.583712 287.577666,298.687128 L292.498754,303.498754" id="Arrow-Right-Copy-2" transform="translate(289.998754, 298.500000) rotate(-90.000000) translate(-289.998754, -298.500000) "></path>
                                          </g>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                           </span>
                        </div>
                        <div class="ha-partner-nav-account-container">
                           <div class="ha-partner-nav-account-item" id="accountSettings"><a class="ha-partner-nav-account-item-link" href="/p/account-settings">Account settings</a></div>
                           <div class="ha-partner-nav-account-item" id="gdListingDetails"><a class="ha-partner-nav-account-item-link" href="/gd/dash/manage-listing/121.7355928.6712262">Property details</a></div>
                           <div class="ha-partner-nav-account-item" id="header-listing-archive"><a class="ha-partner-nav-account-item-link" href="/haod/external/listing-archive.html" target="_blank">Property archive</a></div>
                           <div class="ha-partner-nav-account-item" id="dash-add-property-link"><a class="ha-partner-nav-account-item-link" href="/lyp?addProperty=true">Add new property</a></div>
                           <div class="ha-partner-nav-account-item" id="dash-signout-link"><a class="ha-partner-nav-account-item-link" href="/haod/auth/signout.html">Sign out</a></div>
                        </div>
                     </div>
                     <div class="ha-partner-property-selector-container theme-gt generic ha-partner-header-section ha-partner-property-selected ">
                        <div class="ha-partner-property-selector-item-header">
                           <div class="ha-partner-property-selector-item-header-text">
                              <div class="ha-partner-property-selector-property-text"> US</div>
                              <div class="ha-partner-property-selector-property-details">HA 7355928</div>
                           </div>
                           <span class="ha-partner-property-selector-group-icon ">
                              <svg width="12px" height="6px" viewBox="0 0 12 6" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                 <g stroke="currentColor" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-284.000000, -356.000000)" stroke="#353E44">
                                       <g>
                                          <g transform="translate(0.000000, 60.000000)">
                                             <path d="M292.498754,293.501246 L287.577666,298.313137 C287.47245,298.416288 287.47245,298.583712 287.577666,298.687128 L292.498754,303.498754" id="Arrow-Right-Copy-2" transform="translate(289.998754, 298.500000) rotate(-90.000000) translate(-289.998754, -298.500000) "></path>
                                          </g>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                           </span>
                        </div>
                        <div class="ha-partner-property-selector-group  ha-partner-property-selector-group-collapsed">
                           <div class="ha-partner-property-selector-non-scrollable"></div>
                           <div class="ha-partner-property-selector-sub-item-group   ">
                              <div class="ha-partner-property-selector-more-overlay">No results found ""</div>
                              <div>
                                 <div class="ha-partner-property-selector-sub-item "><span class="ha-partner-property-selector-sub-item-header" title=" 1095 Tully Rd, San Jose, CA, 95122, US"> 1095 Tully Rd, San Jose, CA, 95122, US</span><span class="ha-partner-property-selector-sub-item-listing-details" title="HA 7352502">HA 7352502</span></div>
                                 <div class="ha-partner-property-selector-sub-item "><span class="ha-partner-property-selector-sub-item-header" title=" US"> US</span><span class="ha-partner-property-selector-sub-item-listing-details" title="HA 7355924">HA 7355924</span></div>
                                 <div class="ha-partner-property-selector-sub-item "><span class="ha-partner-property-selector-sub-item-header" title=" US"> US</span><span class="ha-partner-property-selector-sub-item-listing-details" title="HA 7355921">HA 7355921</span></div>
                                 <div class="ha-partner-property-selector-sub-item ha-partner-property-selector-selected-item"><span class="ha-partner-property-selector-sub-item-header" title=" US"> US</span><span class="ha-partner-property-selector-sub-item-listing-details" title="HA 7355928">HA 7355928</span></div>
                                 <div class="ha-partner-property-selector-sub-item "><span class="ha-partner-property-selector-sub-item-header" title=" US"> US</span><span class="ha-partner-property-selector-sub-item-listing-details" title="HA 7355926">HA 7355926</span></div>
                              </div>
                           </div>
                           <div class="ha-partner-property-selector-sub-item ha-partner-property-selector-my-properties-container">
                              <div class="ha-partner-property-selector-sub-item-header"><a class="ha-partner-property-selector-my-properties-link" href="/haod/properties.html" title="See all properties">See all properties</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </header>
            </div>
            <div>
               <nav class="ha-partner-nav-container ha-partner-nav-closed theme-gt generic topclass">
                  <div class="ha-partner-nav-toggle-container"><span class="ha-partner-nav-toggle-hamburger"><span></span><span></span><span></span></span></div>
                  <div class="ha-partner-nav-body-container">
                                     </div>
               </nav>
            </div>
            <div class="property-details step-container">
               <div class="panel-container">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="checklist-progress-bar-container">
                           <div class="checklist-progress-bar">
                              <div class="progress-message">
                                 <div id="message-text">Progress</div>
                              </div>
                              <div class="progress-picture">
                                 <div class="bar done"></div>
                                 <div class="bar not-done barclass" ></div>
                              </div>
                              <hr/>
                           </div>
                           <div class="nav-list-toggle"><i class="icon icon-chevron-down" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 row content-with-nav">
                     <div class="nav-container col-md-3">
                        <div id="checklist-nav-container">
                           <div class="dash-checklist-container nav-dash nav-dash-bce">
                              <ul class="nav-list">
                                 <li class="dash-checklist-item " data-automation-class="summary">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-"></div>
                                       <a href="/pob/checklist/121.7355928.6712262" alt="Welcome" class="dash-checklist-label">Welcome</a>
                                    </span>
                                 </li>
                                 <li class="dash-checklist-item " data-automation-class="location">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-inprogress">
                                          <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-130.000000, -83.000000)">
                                                <g id="Partial" transform="translate(130.000000, 83.000000)" stroke-width="2">
                                                   <rect id="Rectangle" stroke="#353E44" opacity="0.200000018" x="1.05263158" y="1.05263158" width="17.8947368" height="17.8947368" rx="8.94736842"></rect>
                                                   <path d="M10,1.05263158 C14.9414951,1.05263158 18.9473684,5.05850487 18.9473684,10 C18.9473684,14.9414951 14.9414951,18.9473684 10,18.9473684 C5.05850487,18.9473684 1.05263158,14.9414951 1.05263158,10" id="Rectangle" stroke="#228AE6" stroke-linecap="round"></path>
                                                </g>
                                             </g>
                                          </svg>
                                       </div>
                                       <a href="/Locationform" alt="Location" class="dash-checklist-label">Location</a>
                                    </span>
                                 </li>
                                 <li class="dash-checklist-item " data-automation-class="propertyDetails">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-inprogress">
                                          <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-130.000000, -83.000000)">
                                                <g id="Partial" transform="translate(130.000000, 83.000000)" stroke-width="2">
                                                   <rect id="Rectangle" stroke="#353E44" opacity="0.200000018" x="1.05263158" y="1.05263158" width="17.8947368" height="17.8947368" rx="8.94736842"></rect>
                                                   <path d="M10,1.05263158 C14.9414951,1.05263158 18.9473684,5.05850487 18.9473684,10 C18.9473684,14.9414951 14.9414951,18.9473684 10,18.9473684 C5.05850487,18.9473684 1.05263158,14.9414951 1.05263158,10" id="Rectangle" stroke="#228AE6" stroke-linecap="round"></path>
                                                </g>
                                             </g>
                                          </svg>
                                       </div>
                                       <a href="/Detailsform" alt="Details" class="dash-checklist-label">Details</a>
                                    </span>
                                 </li>
                                 <li class="dash-checklist-item " data-automation-class="booking">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-notstarted">
                                          <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-93.000000, -83.000000)" opacity="0.200000018">
                                                <g id="Incomplete" transform="translate(93.000000, 83.000000)" stroke="#353E44" stroke-width="2">
                                                   <rect id="Rectangle-Copy" x="1.05263158" y="1.05263158" width="17.8947368" height="17.8947368" rx="8.94736842"></rect>
                                                </g>
                                             </g>
                                          </svg>
                                       </div>
                                       <a href="/Bookingform" alt="Booking options" class="dash-checklist-label">Booking options</a>
                                    </span>
                                 </li>
                                 <li class="dash-checklist-item " data-automation-class="photos">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-notstarted">
                                          <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-93.000000, -83.000000)" opacity="0.200000018">
                                                <g id="Incomplete" transform="translate(93.000000, 83.000000)" stroke="#353E44" stroke-width="2">
                                                   <rect id="Rectangle-Copy" x="1.05263158" y="1.05263158" width="17.8947368" height="17.8947368" rx="8.94736842"></rect>
                                                </g>
                                             </g>
                                          </svg>
                                       </div>
                                       <a href="/Photo" alt="Photos" class="dash-checklist-label">Photos</a>
                                    </span>
                                 </li>
                                 <li class="dash-checklist-item " data-automation-class="rates">
                                    <span>
                                       <div class="dash-checklist-status checklist-status-notstarted">
                                          <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                             <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-93.000000, -83.000000)" opacity="0.200000018">
                                                <g id="Incomplete" transform="translate(93.000000, 83.000000)" stroke="#353E44" stroke-width="2">
                                                   <rect id="Rectangle-Copy" x="1.05263158" y="1.05263158" width="17.8947368" height="17.8947368" rx="8.94736842"></rect>
                                                </g>
                                             </g>
                                          </svg>
                                       </div>
                                       <a href="/Pricingform" alt="Pricing" class="dash-checklist-label">Pricing</a>
                                    </span>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
            

         </div>
            
            
             </div> 
            
        );
    }
}

export default HeaderOwnerDashboard;














 