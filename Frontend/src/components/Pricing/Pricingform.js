import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { postProperty } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';

class Pricingform extends Component {

  //Define component that you wanbt to render
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }
  /*Action call
  Whenever onSubmit event is triggered, execute an action call called createBook 
  */
  onSubmit(values) {
    
    console.log("values final",values);
    console.log("owner email",localStorage.username);
    values.startdate= localStorage.ownerstartdate;
    values.enddate= localStorage.ownerenddate;
    values.username= localStorage.username;
    
    this.props.postProperty(values, () => {
      this.props.history.push("/OwnerDashboard");
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (

      //handleSubmit is the method that comes from redux and it tells redux what to do with the submitted form data
      //Field is a component of redux that does the wiring of inputs to the redux store.
      <div>
      <HeaderOwnerDashboard />
      <div class="col-xs-6 col-sm-8 content-panel-container">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div>
                        <div class="checklist-header-container ">
                           <h3><span>Verify your rental</span></h3>
                           <hr/>
                        </div>
                        <div>
                           <div></div>

     <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>

     <div><label>Propertytype</label></div>
     <Field label="Currency" 
                name="currency" 
                component="select">
                <option value=""></option>
                <option value="AUD">Australian Dollar (AUD)</option>
                <option value="SGD">Singapore Dollar (SGD)</option>
                <option value="JPY">Japanese Yen (JPY)</option>
                <option value="EUR">Euros (EUR)</option>
                <option value="GBP">Great British Pound (GBP)</option>
                <option value="USD">US Dollar (USD)</option>
                <option value="CAD">Canadian Dollar (CAD)</option>
                <option value="NZD">New Zealand Dollar (NZD)</option>
                <option value="BRL">Brazil Real (BRL)</option>                                         
          </Field>
        
        <Field
          label="Nightly Based Rate"
          name="nightbasedrate"
          component={this.renderField}
        />

        <Field
          label="Minimum stay"
          name="stay"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/Availability" className="btn btn-primary">Back</Link>
      </form>
      </div>
                     </div>
                     <hr/>
                  </div>
               </div>
               </div>
      </div>
    );
  }
}

function validate(values) {

  const errors = {};

  // Validate the inputs from 'values'
  if (!values.address) {
    errors.address = "Enter an address";
  }
  if (!values.city) {
    errors.city = "Enter city";
  }
  if (!values.state) {
    errors.state = "Enter state";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties, redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: "NewBookForm",
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(connect(null, { postProperty })(Pricingform));
