import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './Property.css';
import axios from 'axios';
import CCtest from '../CCtest/CCtest';
import { connect } from 'react-redux';
import DashboardHeader from '../Dashboard/DashboardHeader';

class Property extends React.Component {
    constructor(props){
        super(props);
        this.state={
            todos: this.props.location.state.resultdata,
            bookstate: "Book Now",
            currentPage: 1,
            todosPerPage: 2
        }
        console.log("property search  constructor",this.props.location.state.resultdata);
        this.handleClick = this.handleClick.bind(this);
        this.handlenumberClick = this.handlenumberClick.bind(this);
    }
    handlenumberClick(event) {
        this.setState({
          currentPage: Number(event.target.id)
        });
      }
    
    handleClick(e){
        e.preventDefault();
        // propid: null
        // console.log("propid "+propid);
        console.log("event",e.currentTarget.getAttribute('data-column'));
        const propertyid=e.currentTarget.getAttribute('data-column');
        const data = {
            username : localStorage.username,
            propid : propertyid
        }
        axios.post('/booking',data)
                       .then((response) => {
                           console.log(response.data);
                        // this.setState({
                        //     bookstate: "Booked"
                        // })
                   });
            }
      

     render() {
        const { todos, currentPage, todosPerPage } = this.state;

        // Logic for displaying current todos
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);
        const renderTodos = currentTodos.map((todo, index) => {
            return(
                <div>
                <div class="FilterBar" data-wdio="FilterBar">
                <div data-wdio="PriceDropdown" class="FilterBar__dropdown PriceDropdown FilterBar__dropdown--unselected">
                   <div class="Dropdown" tabindex="-1" role="presentation">
                      <button class="btn btn-default Dropdown__toggle border-border-color" id="priceFilter" aria-haspopup="true" label="Price" aria-expanded="false" type="button">
                         <span class="btn__label">Price</span>
                         <span class="SVGIcon SVGIcon--12px Dropdown__toggle-icon text-link">
                            <svg data-id="SVG_CHEVRON_DOWN__12" width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
                               <path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M11 4L6 9 1 4"></path>
                            </svg>
                         </span>
                      </button>
                      <ul class="Dropdown__menu"  role="menu" aria-labelledby="priceFilter">
                         <div class="DropdownPanel">
                            <div class="DropdownPanel__content">
                               <div class="DropdownPanel__children">
                                  <div class="PriceSlider">
                                     <div class="PriceSlider__priceLabel"><span class="PriceSlider__priceLabel__values"><span>Any price</span></span><span class="PriceSlider__priceLabel__perNight"><span>per night</span></span></div>
                                     <div class="PriceSlider__slider" data-wdio="PriceSlider__slider">
                                        <div class="rc-slider Slider">
                                           <div class="rc-slider-rail"></div>
                                           <div class="rc-slider-track rc-slider-track-1 tracksty" ></div>
                                           <div class="rc-slider-step"></div>
                                           <div class="rc-slider-handle-target slidersty" role="slider" tabindex="0" data-wdio="rc-slider-handle-target-0" >
                                              <div class="rc-slider-handle rc-slider-handle-1"></div>
                                           </div>
                                           <div class="rc-slider-handle-target handlesty" role="slider" tabindex="0" data-wdio="rc-slider-handle-target-1" aria-valuemin="0" aria-valuemax="1000" aria-valuenow="1000" aria-disabled="false" >
                                              <div class="rc-slider-handle rc-slider-handle-2"></div>
                                           </div>
                                           <div class="rc-slider-mark"></div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               <div class="DropdownPanel__buttons"><button class="btn btn-primary btn-rounded btn-xs" data-wdio="DropdownPanel-apply-button" label="Apply" type="button"><span class="btn__label">Apply</span></button></div>
                            </div>
                         </div>
                      </ul>
                   </div>
                </div>
                <div data-wdio="BedroomsDropdown" class="FilterBar__dropdown BedroomsDropdown FilterBar__dropdown--unselected">
                   <div class="Dropdown" tabindex="-1" role="presentation">
                      <button class="btn btn-default Dropdown__toggle border-border-color" id="bedroomsFilter" aria-haspopup="true" label="Bedrooms" aria-expanded="false" type="button">
                         <span class="btn__label">Bedrooms</span>
                         <span class="SVGIcon SVGIcon--12px Dropdown__toggle-icon text-link">
                            <svg data-id="SVG_CHEVRON_DOWN__12" width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
                               <path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M11 4L6 9 1 4"></path>
                            </svg>
                         </span>
                      </button>
                      <ul class="Dropdown__menu"  role="menu" aria-labelledby="bedroomsFilter">
                         <div class="DropdownPanel">
                            <div class="DropdownPanel__content">
                               <div class="DropdownPanel__children">
                                  <div class="BedroomsFilter" data-wdio="BedroomsFilter">
                                     <span class="BedroomsFilter--icon" aria-hidden="true">
                                        <span>
                                           <svg display="none" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                              <defs>
                                                 <symbol id="svg-plus" viewBox="0 0 50 50">
                                                    <title>plus</title>
                                                    <path d="M50 22H28V0h-6v22H0v6h22v22h6V28h22"></path>
                                                 </symbol>
                                              </defs>
                                           </svg>
                                        </span>
                                     </span>
                                     <span class="BedroomsFilter--icon" aria-hidden="true">
                                        <span>
                                           <svg display="none" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                              <defs>
                                                 <symbol id="svg-minus" viewBox="0 0 50 50">
                                                    <title>minus</title>
                                                    <path d="M0 22.7h50v4.5H0z"></path>
                                                 </symbol>
                                              </defs>
                                           </svg>
                                        </span>
                                     </span>
                                     <div class="BedroomsFilter__QuantitySelector BedroomsFilter--row form-group">
                                        <div class="quantity-selector" step="1">
                                           <label aria-live="polite" class="quantity-selector__label" for="bedrooms_filter-quantity">
                                              <span class="quantity-selector__custom-label">
                                                 <div><span class="BedroomsFilter__value">Any</span><span class="BedroomsFilter__label">Bedrooms</span></div>
                                              </span>
                                              <input type="number" min="0" max="50" class="quantity-selector__input" id="bedrooms_filter-quantity" value="0" />
                                              <div class="quantity-selector__focus-indicator bg-brand"></div>
                                           </label>
                                           <button class="btn btn-icon btn-default quantity-selector__btn quantity-selector__btn--decrease btn-icon-circle" disabled="" label="Decrease bedrooms" type="button">
                                              <span class="btn__label sr-only">Decrease bedrooms</span>
                                              <span class="SVGIcon SVGIcon--16px flex-center">
                                                 <svg data-id="SVG_MINUS__16" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15 8H1" fill="none" fill-rule="evenodd" stroke-linecap="round"></path>
                                                 </svg>
                                              </span>
                                           </button>
                                           <button class="btn btn-icon btn-default quantity-selector__btn quantity-selector__btn--increase btn-icon-circle" label="Increase bedrooms" type="button">
                                              <span class="btn__label sr-only">Increase bedrooms</span>
                                              <span class="SVGIcon SVGIcon--16px flex-center">
                                                 <svg data-id="SVG_PLUS__16" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                    <g fill="none" fill-rule="evenodd" stroke-linecap="round">
                                                       <path d="M8 1v14M15 8H1"></path>
                                                    </g>
                                                 </svg>
                                              </span>
                                           </button>
                                        </div>
                                     </div>
                                     <div class="BedroomsFilter__Switch BedroomsFilter--row form-group">
                                        <div class="switch" role="switch" aria-checked="false">
                                           <input type="checkbox" id="bedrooms_switch" name="exact bedrooms" />
                                           <label for="bedrooms_switch">
                                              <span class="SVGIcon SVGIcon--24px SVGIcon--inline switch-checked text-white">
                                                 <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                 </svg>
                                              </span>
                                              <span class="SVGIcon SVGIcon--24px SVGIcon--inline switch-unchecked text-white">
                                                 <svg data-id="SVG_CLOSE__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" d="M1 15L15 1m0 14L1 1" fill="none" stroke-linecap="round" stroke-linejoin="round"></path>
                                                 </svg>
                                              </span>
                                              <div class="switch-toggle"></div>
                                           </label>
                                           <label for="bedrooms_switch"><span>Use exact match for bedrooms</span></label>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               <div class="DropdownPanel__buttons"><button class="btn btn-primary btn-rounded btn-xs" data-wdio="DropdownPanel-apply-button" label="Apply" type="button"><span class="btn__label">Apply</span></button></div>
                            </div>
                         </div>
                      </ul>
                   </div>
                </div>
                <div data-wdio="InstantConfirmationDropdown" class="FilterBar__dropdown InstantConfirmationDropdown FilterBar__dropdown--unselected">
                   <div class="Dropdown" tabindex="-1" role="presentation">
                      <button class="btn btn-default Dropdown__toggle border-border-color" id="instantConfirmationFilter" aria-haspopup="true" label="Instant Confirmation" aria-expanded="false" type="button">
                         <span class="btn__label">Instant Confirmation</span>
                         <span class="SVGIcon SVGIcon--12px Dropdown__toggle-icon text-link">
                            <svg data-id="SVG_CHEVRON_DOWN__12" width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
                               <path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M11 4L6 9 1 4"></path>
                            </svg>
                         </span>
                      </button>
                      <ul class="Dropdown__menu"  role="menu" aria-labelledby="instantConfirmationFilter">
                         <div class="DropdownPanel">
                            <div class="DropdownPanel__content">
                               <div class="DropdownPanel__children">
                                  <div class="InstantConfirmationFilter" data-wdio="InstantConfirmationFilter">
                                     <div class="InstantConfirmationFilter--row form-group">
                                        <div class="InstantConfirmation__primaryLabel">
                                           <span class="InstantBook__icon" aria-hidden="true">
                                              <span>
                                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path d="M6.9,8.9l-0.5,5.9c0,0.6,0.2,0.7,0.5,0.2l5.6-7c0.3-0.4,0.2-0.8-0.4-0.8h-3l0.5-5.9 c0-0.6-0.2-0.7-0.5-0.2l-5.6,7C3.1,8.5,3.3,8.9,3.9,8.9H6.9z"></path>
                                                 </svg>
                                              </span>
                                           </span>
                                           <span>Instant Confirmation</span>
                                        </div>
                                        <div class="InstantConfirmation__secondaryLabel"><span>Book without waiting for owner approval</span></div>
                                     </div>
                                     <div class="InstantConfirmationFilter--row form-group" data-wdio="InstantConfirmationFilter__toggle">
                                        <div class="switch" role="switch" aria-checked="false">
                                           <input type="checkbox" id="instantConfirmation__switch" name="filter:89"/>
                                           <label for="instantConfirmation__switch">
                                              <span class="SVGIcon SVGIcon--24px SVGIcon--inline switch-checked text-white">
                                                 <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                 </svg>
                                              </span>
                                              <span class="SVGIcon SVGIcon--24px SVGIcon--inline switch-unchecked text-white">
                                                 <svg data-id="SVG_CLOSE__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" d="M1 15L15 1m0 14L1 1" fill="none" stroke-linecap="round" stroke-linejoin="round"></path>
                                                 </svg>
                                              </span>
                                              <div class="switch-toggle"></div>
                                           </label>
                                           <label for="instantConfirmation__switch"></label>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </ul>
                   </div>
                </div>
                <div class="FilterBar__dropdown MoreFiltersPanelDropdown FilterBar__dropdown--unselected">
                   <div class="Dropdown" tabindex="-1" role="presentation">
                      <button class="btn btn-default Dropdown__toggle border-border-color" id="moreFilters" aria-haspopup="true" label="More Filters " aria-expanded="false" type="button">
                         <span class="btn__label">More Filters </span>
                         <span class="SVGIcon SVGIcon--12px Dropdown__toggle-icon text-link">
                            <svg data-id="SVG_CHEVRON_DOWN__12" width="12" height="12" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
                               <path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M11 4L6 9 1 4"></path>
                            </svg>
                         </span>
                      </button>
                      <ul class="Dropdown__menu"  role="menu" aria-labelledby="moreFilters">
                         <div class="MoreFiltersPanel" data-wdio="MoreFiltersPanel">
                            <div class="MoreFiltersPanel__content">
                               <div class="MoreFiltersPanel__column--left MoreFiltersPanel__column">
                                  <div class="MoreFiltersPanel__filter-section MoreFiltersPanel__filter-section--bordered">
                                     <div class="BathroomsFilter" data-wdio="BathroomsFilter">
                                        <span class="BathroomsFilter__icon" aria-hidden="true">
                                           <span>
                                              <svg display="none" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                                 <defs>
                                                    <symbol id="svg-plus" viewBox="0 0 50 50">
                                                       <title>plus</title>
                                                       <path d="M50 22H28V0h-6v22H0v6h22v22h6V28h22"></path>
                                                    </symbol>
                                                 </defs>
                                              </svg>
                                           </span>
                                        </span>
                                        <span class="BathroomsFilter__icon" aria-hidden="true">
                                           <span>
                                              <svg display="none" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                                 <defs>
                                                    <symbol id="svg-minus" viewBox="0 0 50 50">
                                                       <title>minus</title>
                                                       <path d="M0 22.7h50v4.5H0z"></path>
                                                    </symbol>
                                                 </defs>
                                              </svg>
                                           </span>
                                        </span>
                                        <div class="BathroomsFilter__QuantitySelector BathroomsFilter__row form-group">
                                           <div class="quantity-selector" step="1">
                                              <label aria-live="polite" class="quantity-selector__label" for="BathroomsFilter__quantity">
                                                 <span class="quantity-selector__custom-label">
                                                    <div><span class="BathroomsFilter__value">Any</span><span class="BathroomsFilter__label">Bathrooms</span></div>
                                                 </span>
                                                 <input type="number" min="0" max="50" class="quantity-selector__input" id="BathroomsFilter__quantity" value="0"/>
                                                 <div class="quantity-selector__focus-indicator bg-brand"></div>
                                              </label>
                                              <button class="btn btn-icon btn-default quantity-selector__btn quantity-selector__btn--decrease btn-icon-circle" disabled="" label="Decrease bathrooms" type="button">
                                                 <span class="btn__label sr-only">Decrease bathrooms</span>
                                                 <span class="SVGIcon SVGIcon--16px flex-center">
                                                    <svg data-id="SVG_MINUS__16" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                       <path d="M15 8H1" fill="none" fill-rule="evenodd" stroke-linecap="round"></path>
                                                    </svg>
                                                 </span>
                                              </button>
                                              <button class="btn btn-icon btn-default quantity-selector__btn quantity-selector__btn--increase btn-icon-circle" label="Increase bathrooms" type="button">
                                                 <span class="btn__label sr-only">Increase bathrooms</span>
                                                 <span class="SVGIcon SVGIcon--16px flex-center">
                                                    <svg data-id="SVG_PLUS__16" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                       <g fill="none" fill-rule="evenodd" stroke-linecap="round">
                                                          <path d="M8 1v14M15 8H1"></path>
                                                       </g>
                                                    </svg>
                                                 </span>
                                              </button>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_3">
                                     <div class="MoreFilters__filterGroup" data-wdio="MoreorLess_3">
                                        <div id="MoreFilters--3" class="MoreOrLess MoreOrLess--block">
                                           <div  class="CollapseBase basesty">
                                              <div class="CollapseBase__content">
                                                 <div class="FilterGroup">
                                                    <h3 class="FilterGroup__label">Location</h3>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:30" name="filter:30"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Beach</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:42" name="filter:42"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Waterfront</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:37" name="filter:37"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Ocean</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:91" name="filter:91"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Oceanfront</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:32" name="filter:32"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Beachfront</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:31" name="filter:31"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Beach view</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:35" name="filter:35"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Lake</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:36" name="filter:36"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Mountains</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:33" name="filter:33"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Downtown</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:39" name="filter:39" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Ski-in/ski-out</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:40" name="filter:40" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Town</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:38" name="filter:38"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Rural</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:41" name="filter:41"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Village</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:34" name="filter:34"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Golf course</span>
                                                       </label>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           <button aria-expanded="false" aria-controls="MoreFilters--3" aria-hidden="true" class="text-link MoreOrLess__toggle MoreOrLess__toggle--block"><span class="MoreOrLess__toggle-text"><span>More</span></span></button>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_4">
                                     <div class="MoreFilters__filterGroup">
                                        <div class="FilterGroup">
                                           <h3 class="FilterGroup__label">Properties good for</h3>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:43" name="filter:43"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Families</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:44" name="filter:44"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Luxury</span>
                                              </label>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_7">
                                     <div class="MoreFilters__filterGroup">
                                        <div class="FilterGroup">
                                           <h3 class="FilterGroup__label">Safety &amp; accessibility</h3>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:86" name="filter:86"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Low-allergen environment</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:87" name="filter:87"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Wheelchair accessible</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:85" name="filter:85"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Elevator</span>
                                              </label>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_8">
                                     <div class="MoreFilters__filterGroup">
                                        <div class="FilterGroup">
                                           <h3 class="FilterGroup__label">Services</h3>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:88" name="filter:88"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Housekeeping</span>
                                              </label>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               <div class="MoreFiltersPanel__column--right MoreFiltersPanel__column">
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_6">
                                     <div class="MoreFilters__filterGroup" data-wdio="MoreorLess_6">
                                        <div id="MoreFilters--6" class="MoreOrLess MoreOrLess--block">
                                           <div  class="CollapseBase basesty">
                                              <div class="CollapseBase__content">
                                                 <div class="FilterGroup">
                                                    <h3 class="FilterGroup__label">Property type</h3>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:74" name="filter:74"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">House</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:63" name="filter:63"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Bungalow</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:64" name="filter:64"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Cabin</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:69" name="filter:69"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Cottage</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:80" name="filter:80" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Studio</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:62" name="filter:62" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Boat</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:92" name="filter:92"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Apartment/condo</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:83" name="filter:83"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Villa</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:76" name="filter:76" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Lodge</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:66" name="filter:66" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Chalet</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:82" name="filter:82"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Townhouse</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:75" name="filter:75" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Houseboat</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:71" name="filter:71" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Farmhouse</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:84" name="filter:84" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Yacht</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:67" name="filter:67" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Chateau/country house</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:60" name="filter:60" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Barn</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:70" name="filter:70" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Estate</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:77" name="filter:77" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Mill</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:65" name="filter:65" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Castle</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:78" name="filter:78" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Mobile home/caravan</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:72" name="filter:72" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Guest house</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:73" name="filter:73"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Hotel</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:81" name="filter:81" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Tower</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:79" name="filter:79"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Resort</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group disabled" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:61" name="filter:61" disabled=""/>
                                                          <div class="toggle"></div>
                                                          <span class="label-text">Bed &amp; breakfast</span>
                                                       </label>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           <button aria-expanded="false" aria-controls="MoreFilters--6" aria-hidden="true" class="text-link MoreOrLess__toggle MoreOrLess__toggle--block"><span class="MoreOrLess__toggle-text"><span>More</span></span></button>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_1">
                                     <div class="MoreFilters__filterGroup" data-wdio="MoreorLess_1">
                                        <div id="MoreFilters--1" class="MoreOrLess MoreOrLess--block">
                                           <div  class="CollapseBase basesty">
                                              <div class="CollapseBase__content">
                                                 <div class="FilterGroup">
                                                    <h3 class="FilterGroup__label">Features &amp; amenities</h3>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:23" name="filter:23"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Pool</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:10" name="filter:10"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Heated/indoor pool</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:12" name="filter:12"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Hot tub</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:24" name="filter:24"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Private pool</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:13" name="filter:13"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Internet/WiFi</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:1" name="filter:1"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Air conditioning</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:19" name="filter:19"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Outdoor grill/BBQ</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:21" name="filter:21"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Parking available</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:17" name="filter:17"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Laundry machines</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:8" name="filter:8"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Garden or backyard</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:25" name="filter:25"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">TV</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:5" name="filter:5"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Dishwasher</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:3" name="filter:3"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Bed linens provided</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:7" name="filter:7"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Fireplace</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:2" name="filter:2"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Balcony</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:22" name="filter:22"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Patio or deck</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:20" name="filter:20"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Oven/stove</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:11" name="filter:11"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Heating</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:15" name="filter:15"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Kids' high chair</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:4" name="filter:4"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Crib</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:18" name="filter:18"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Microwave</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:6" name="filter:6"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">DVD player</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:14" name="filter:14"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Iron &amp; ironing board</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:9" name="filter:9"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Gym/fitness equipment</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:16" name="filter:16"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">King-sized bed</span>
                                                       </label>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           <button aria-expanded="false" aria-controls="MoreFilters--1" aria-hidden="true" class="text-link MoreOrLess__toggle MoreOrLess__toggle--block"><span class="MoreOrLess__toggle-text"><span>More</span></span></button>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_amenities_title">
                                     <div class="MoreFilters__filterGroup">
                                        <div class="FilterGroup">
                                           <h3 class="FilterGroup__label">House rules</h3>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:27" name="filter:27"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Pets allowed</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:29" name="filter:29"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Children welcome</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:28" name="filter:28"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Smoking allowed</span>
                                              </label>
                                           </div>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:26" name="filter:26"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">Events allowed</span>
                                              </label>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_5">
                                     <div class="MoreFilters__filterGroup" data-wdio="MoreorLess_5">
                                        <div id="MoreFilters--5" class="MoreOrLess MoreOrLess--block">
                                           <div class="CollapseBase basesty">
                                              <div class="CollapseBase__content">
                                                 <div class="FilterGroup">
                                                    <h3 class="FilterGroup__label">Nearby activities</h3>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:55" name="filter:55"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Skiing/snowboarding</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:48" name="filter:48"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Golfing</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:47" name="filter:47"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Fishing</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:49" name="filter:49"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Hiking</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:50" name="filter:50"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Horseback riding</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:45" name="filter:45"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Amusement/theme parks</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:46" name="filter:46"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Cycling</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:56" name="filter:56"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Tennis</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:52" name="filter:52"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Rock/mountain climbing</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:58" name="filter:58"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Winery/brewery tours</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:54" name="filter:54"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Shopping &amp; relaxation</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:57" name="filter:57"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Watersports</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:51" name="filter:51"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Museums &amp; galleries</span>
                                                       </label>
                                                    </div>
                                                    <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                                       <label>
                                                          <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:53" name="filter:53"/>
                                                          <div class="toggle">
                                                             <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                                <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                   <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                                </svg>
                                                             </span>
                                                          </div>
                                                          <span class="label-text">Scuba diving/snorkeling</span>
                                                       </label>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           <button aria-expanded="false" aria-controls="MoreFilters--5" aria-hidden="true" class="text-link MoreOrLess__toggle MoreOrLess__toggle--block"><span class="MoreOrLess__toggle-text"><span>More</span></span></button>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="MoreFiltersPanelGroup" data-wdio="MoreFilterGroup_booking_title">
                                     <div class="MoreFilters__filterGroup">
                                        <div class="FilterGroup">
                                           <h3 class="FilterGroup__label">Booking Options</h3>
                                           <div class="form-group checkbox-group" role="checkbox" aria-checked="false">
                                              <label>
                                                 <input type="checkbox" class="checkbox" data-wdio="checkbox__filter:90" name="filter:90"/>
                                                 <div class="toggle">
                                                    <span class="SVGIcon SVGIcon--24px SVGIcon--inline text-white">
                                                       <svg data-id="SVG_CHECK__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                          <path transform="scale(1.5, 1.5)" vector-effect="non-scaling-stroke" fill="none" stroke-linecap="round" stroke-linejoin="round" d="M1 9.5L5.7 14 15 2"></path>
                                                       </svg>
                                                    </span>
                                                 </div>
                                                 <span class="label-text">24 Hour Confirmation</span>
                                              </label>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                            <div class="MoreFiltersPanel__buttons"><button class="btn btn-default btn-rounded" id="moreFiltersPanel__btn--cancel" label="Cancel" type="button"><span class="btn__label">Cancel</span></button><button class="btn btn-primary btn-rounded" id="moreFiltersPanel__btn--submit" label="See 110 Results" type="button"><span class="btn__label">See 110 Results</span></button></div>
                         </div>
                      </ul>
                   </div>
                </div>
             </div>
             <div class="Hit media-flex media-flex--left media-flex--xs" data-wdio="hit">
                <div threshold="1200" class="HitCarousel thumbnail--noMargin Hit__thumbnail content--clickable">
                   <CCtest propertyid={todo.propid}/>
                   <div class="Hit__tripboardButton">
                      <button class="react-trip-board-button react-trip-board-button__position-right" tabindex="-1" aria-label="Create a new Trip Board">
                         <div class="react-trip-board-button__heart-core">
                            <svg height="40px" viewBox="-10 -12 36 35" width="40px">
                               <path class="react-trip-board-button__heart-core-bg react-trip-board-button__heart-core__heart-icon unhearted" d="M14.1294135,7.57785915 L9.1813451,12.5257519 C8.40187017,13.3051991 7.13698698,13.3040721 6.35866678,12.5257519 L1.41077405,7.57785915 C0.543404248,6.78002342 -8.8817842e-16,5.63564613 -8.8817842e-16,4.3646175 C-8.8817842e-16,1.9541783 1.95395254,0 4.3646175,0 C5.60449125,0 6.72403494,0.517216091 7.51825852,1.3473355 C7.60675643,1.43944557 7.69073914,1.53539356 7.76998089,1.63472794 C7.84989992,1.53539356 7.93365686,1.43944557 8.02170325,1.3473355 C8.81615259,0.517216091 9.93569628,0 11.1753443,0 C13.5860092,8.02060533e-16 15.5399618,1.9541783 15.5399618,4.3646175 C15.5399618,5.63564613 14.9965575,6.78024918 14.1294135,7.57785915 Z"></path>
                               <path class="react-trip-board-button__heart-core__heart-icon unhearted" d="M14.1294135,7.57785915 L9.1813451,12.5257519 C8.40187017,13.3051991 7.13698698,13.3040721 6.35866678,12.5257519 L1.41077405,7.57785915 C0.543404248,6.78002342 -8.8817842e-16,5.63564613 -8.8817842e-16,4.3646175 C-8.8817842e-16,1.9541783 1.95395254,0 4.3646175,0 C5.60449125,0 6.72403494,0.517216091 7.51825852,1.3473355 C7.60675643,1.43944557 7.69073914,1.53539356 7.76998089,1.63472794 C7.84989992,1.53539356 7.93365686,1.43944557 8.02170325,1.3473355 C8.81615259,0.517216091 9.93569628,0 11.1753443,0 C13.5860092,8.02060533e-16 15.5399618,1.9541783 15.5399618,4.3646175 C15.5399618,5.63564613 14.9965575,6.78024918 14.1294135,7.57785915 Z"></path>
                            </svg>
                         </div>
                      </button>
                   </div>
                    {/* <CCtest ></CCtest> */}
                      {/* <div class="SimpleImageCarousel__image SimpleImageCarousel__image--cur imgsty" role="img" aria-label="" ></div> */}
                      <div class="SimpleImageCarousel__image SimpleImageCarousel__image--next-slide imst" ></div>
                      <div class="SimpleImageCarousel__image SimpleImageCarousel__image--prev-slide"></div>
                      <div class="SimpleImageCarousel__preloader"></div>
                </div>
                <div class="Hit__info">
                   <a target="_blank" class="a--plain-link Hit__infoLink" href="/vacation-rental/p590589vb">
                      <div class="HitInfo HitInfo--desktop">
                         <div class="HitInfo__content">
                            <div class="HitInfo__viewedUrgency hidden-xs" data-wdio="viewed-urgency-message"><small></small></div>
                            <h4 class="HitInfo__headline hover-text " >{todo.headline}</h4>
                            <div class="HitInfo__details">
                               <div class="Details__propertyType Details__label">House</div>
                               <div class="Details__bedrooms Details__label">
                                {todo.bedrooms}BR
                               </div>
                               <div class="Details__bathrooms text-capitalize Details__label">
                                {todo.bathrooms}BA
                               </div>
                               <div class="Details__sleeps Details__label">Sleeps {todo.stay}</div>
                               <div class="Details__area Details__item Details__label"><span class="Details__value">2100</span><span class="text-capitalize">Sq. Ft.</span></div>
                               <div class="Details__label">Full Kitchen</div>
                               <div class="Details__label">Garden</div>
                               <div class="Details__label">Internet</div>
                               <div class="Details__label">Washer &amp; Dryer</div>
                               <div class="Details__label">Parking</div>
                               <div class="Details__label">Bed linens provided</div>
                               <div class="Details__label">Satellite or Cable</div>
                               <div class="Details__label">No Smoking</div>
                               <div class="Details__label">Kids Welcome</div>
                               <div class="Details__label">TV</div>
                               <div class="Details__label">Pets Welcome</div>
                            </div>
                         </div>
                         <div class="HitInfo__infoBar hover-bg">
                            <div class="HitInfo__priceSuperlativeGroup">
                               <span class="HitInfo__superlative small"><strong>Wonderful!</strong> <span>4.9/5</span></span>
                               <div class="HitInfo__price">
                                  <span class="InstantBook__icon" aria-hidden="true" title="Book without waiting for owner approval">
                                     <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                           <path d="M6.9,8.9l-0.5,5.9c0,0.6,0.2,0.7,0.5,0.2l5.6-7c0.3-0.4,0.2-0.8-0.4-0.8h-3l0.5-5.9 c0-0.6-0.2-0.7-0.5-0.2l-5.6,7C3.1,8.5,3.3,8.9,3.9,8.9H6.9z"></path>
                                        </svg>
                                     </span>
                                  </span>
                                  <span class="Price"><span class="Price__value" data-wdio="Price" data-price="343">${todo.nightbasedrate}</span><span class="Price__period">avg/night</span></span>
                               </div>
                            </div>
                            <div class="HitInfo__badgeRatingGroup">
                               <span class="sty"  role="button" tabindex="0">
                               <button type="button" class="btn btn-primary" data-column={todo.propid} onClick={this.handleClick} >{this.state.bookstate}</button>
                               </span>
                            </div>
                         </div>
                      </div>
                   </a> 
                </div>
             </div>
             </div>    
                
            )
        })
   
        

const pageNumbers = [];
for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
  pageNumbers.push(i);
}




const renderPageNumbers = pageNumbers.map(number => {
    return (
      <li
        key={number}
        id={number}
        onClick={this.handlenumberClick}
      >
        {number}
      </li>
    );
  });
        return(
     <div>
     <div>
     <DashboardHeader />
      {renderTodos}
     </div>
         <div id ="footer">
         <footer>
         <ul id="page-numbers">
         {renderPageNumbers}
         </ul>
         </footer></div>
              </div>
     
        );
    }
}


 


export default connect(null)(Property);
