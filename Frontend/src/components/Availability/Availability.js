import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './Availability.css';
import axios from 'axios';
import moment from 'moment';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
//import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
import DatePicker from 'react-datepicker';
import {Redirect} from 'react-router';

class Availability extends React.Component {
    constructor(props)
    {
        super(props);
       console.log(this.props);
         this.state={
             switch: false,
             startDate: moment(),
             endDate: moment()
         }
         this.handlestartDateChange = this.handlestartDateChange.bind(this);
         this.handleendDateChange = this.handleendDateChange.bind(this);
         this.handlenext = this.handlenext.bind(this);
 
    }
    handlenext = (e) => {
          this.setState({
               switch : true
           })
         }

      handlestartDateChange(date) {
        this.setState({
          startDate: date
        });
      }



      handleendDateChange(date) {
        this.setState({
          endDate: date
        });
      }

     render() {
        if(this.state.switch){
            console.log("I am in render to redirect");
            if(this.state.switch){
              localStorage.setItem("ownerstartdate", this.state.startDate);
              localStorage.setItem("ownerenddate", this.state.endDate);
                return <Redirect to={{pathname: '/Pricingform'
                }} />;}    
                //return <Redirect to={{pathname: '/Pricing'}}/>;}
        }
        
        return(
            <div>
            <HeaderOwnerDashboard />
            <div id="layout" class="layout">
   <div data-reactroot="">
      <div class="lodgingrates-sae-page">         
         <div></div>
         <div></div>
            <div class="form-group">
               <div class="more-info"><label class="header-desc-label">Already know when you would like your property to be available?<br/> You can also make changes after publishing your listing.</label></div>
            </div>
            <div class="row">
               <div class="panel panel-dashboard onboarding-page col-md-9">
                  <div class="panel-body">
                     <div class="availability-type">
                        <h3>Select a starting point for setting up your availability</h3>
                        <div class="box-selection undefined">
                           <div class="box-selection-wrapper">
                              <div class="col-xs-6 col-sm-6 box-selection-item">
                                 <div class="mostly-available">
                                    <p>Full calendar availability</p>
                                    <label>Block out certain dates</label>
                                    <div class="availability-icon mostly-available-icon"></div>
                                    <p>Perfect for full time rental properties or super flexible owners</p>
                                    <span><label>Start Date</label><DatePicker  selected={this.state.startDate}
                                    onSelect={this.handleSelect} 
                                     onChange={this.handlestartDateChange} 
                                    /><br/>
                                   <span> <label>EndDate</label>
                                    <DatePicker className="endDateclass" selected={this.state.endDate}
                                    onSelect={this.handleSelect} 
                                     onChange={this.handleendDateChange} 
                                    /> </span></span>

                                 </div>
                              </div>
                              <div class="col-xs-6 col-sm-6 box-selection-item" onClick={this.handlecalenderclick}>
                                 <div class="mostly-blocked">
                                    <p>Blocked calendar</p>
                                    <label>Select the dates your property will be available</label>
                                    <div class="availability-icon mostly-blocked-icon"></div>
                                    <p>Perfect for only listing a property during specific events or seasons</p>
                                    <span><label>Start Date</label><DatePicker  selected={this.state.startDate}
                                    onSelect={this.handleSelect} 
                                     onChange={this.handlestartDateChange} 
                                    /><br/>
                                   <span> <label>EndDate</label>
                                    <DatePicker className="endDateclass" selected={this.state.endDate}
                                    onSelect={this.handleSelect} 
                                     onChange={this.handlestartDateChange} 
                                    /> </span></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div>
                     <div class="bottom-nav">
                        <div class="single-nav-button">
                        {/* <button class="btn btn-primary nav-footer-next btn-wide first-step btn-rounded" id="sae-save-button" type="button" label="Next" onClick={this.handlecalenderclick} > */}
                        <div class="col-xs-6"><button class="btn btn-primary btn-rounded btn-sm"  type="button" onClick={this.handlenext}>Next</button></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

      </div>
   </div>
</div>
      
            </div>
        )
    }
}
export default connect(null)(Availability);
