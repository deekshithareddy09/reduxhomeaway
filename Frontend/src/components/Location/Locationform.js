import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';

class Locationform extends Component {

  //Define component that you wanbt to render
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }
  /*Action call
  Whenever onSubmit event is triggered, execute an action call called createBook 
  */
  onSubmit(values) {
    console.log("values",values.city);
    if (localStorage.clickcount) {
      localStorage.clickcount = Number(localStorage.clickcount)+1;
  } else {
      localStorage.clickcount = 1;
  }
  console.log(localStorage.clickcount);
  console.log(values.city+localStorage.clickcount);
  localStorage.propid=values.city+localStorage.clickcount;
  console.log(localStorage.propid);
    values.propid=localStorage.propid;
    console.log("prp values",values.propid);
    this.props.createLocation(values, () => {
      this.props.history.push("/Detailsform");
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (

      //handleSubmit is the method that comes from redux and it tells redux what to do with the submitted form data
      //Field is a component of redux that does the wiring of inputs to the redux store.
      <div>
      <HeaderOwnerDashboard />
      <div class="col-xs-6 col-sm-8 content-panel-container">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div>
                        <div class="checklist-header-container ">
                           <h3><span>Verify your rental</span></h3>
                           <hr/>
                        </div>
                        <div>
                           <div></div>

     <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
      
        <Field
          label="Address"
          name="address"
          component={this.renderField}
        />

        <Field
          label="City"
          name="city"
          component={this.renderField}
        />
        <Field
          label="State"
          name="state"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/" className="btn btn-danger">Back</Link>
      </form>
      </div>
                     </div>
                     <hr/>
                  </div>
               </div>
               </div>
      </div>
    );
  }
}

function validate(values) {

  const errors = {};

  // Validate the inputs from 'values'
  if (!values.address) {
    errors.address = "Enter an address";
  }
  if (!values.city) {
    errors.city = "Enter city";
  }
  if (!values.state) {
    errors.state = "Enter state";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties, redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: "NewBookForm",
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(connect(null, { createLocation })(Locationform));
