import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';

class Bookingform extends Component {

  //Define component that you wanbt to render
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="radio" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }



  
  /*Action call
  Whenever onSubmit event is triggered, execute an action call called createBook 
  */
  onSubmit(values) {
    console.log("values",values);
    this.props.createLocation(values, () => {
      this.props.history.push("/Photo");
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (

      //handleSubmit is the method that comes from redux and it tells redux what to do with the submitted form data
      //Field is a component of redux that does the wiring of inputs to the redux store.
      <div>
      <HeaderOwnerDashboard />
      <div class="col-xs-6 col-sm-8 content-panel-container">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div>
                        <div class="checklist-header-container ">
                           <h3><span>Verify your rental</span></h3>
                           <hr/>
                        </div>
                        <div>
                           <div></div>

     <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
     <label><Field name="instantbooking" component="input" type="radio"  value="INSTANT"/>Instant Booking
    </label>
    <p><span>Automatically accept booking requests from all travelers for dates you have available, and add the bookings to your calendar. </span></p>

     <label><Field name="24hourreview" component="input" type="radio" value="24Hour"/> 24-hour review
    </label>
    <p><span>Allow 24 hours to communicate with guests and accept booking requests. </span></p>


        
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/Detailsform" className="btn btn-danger">Back</Link>
      </form>
      </div>
                     </div>
                     <hr/>
                  </div>
               </div>
               </div>
      </div>
    );
  }
}

function validate(values) {

  const errors = {};

  // Validate the inputs from 'values'
  if (!values.headline) {
    errors.headline = "Enter an headline";
  }
  if (!values.description) {
    errors.description = "Enter description";
  }
  if (!values.bedrooms) {
    errors.bedrooms = "Enter bedrooms";
  }
  if (!values.bathrooms) {
    errors.bathrooms = "Enter bathrooms";
  }
  if (!values.accomodates) {
    errors.accomodates = "Enter accomodates";
  }




  // If errors is empty, the form is fine to submit
  // If errors has *any* properties, redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: "NewBookForm",
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(connect(null, { createLocation })(Bookingform));
