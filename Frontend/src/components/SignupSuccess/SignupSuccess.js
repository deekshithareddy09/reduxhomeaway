import React from 'react';
//import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import './SignupSucess.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import { connect } from 'react-redux';

class SignupSuccess extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props);
        // console.log(this.props.location.state.firstname+this.props.location.state.lastname);
        // this.state = {
        //     switch: false
        //   };
    }
    
    handleprofile = (e) => {
        this.setState({
             switch: true
         })
       }
    render() {   
        // if(this.state.switch){
        //     return <Redirect to={{pathname: '/ProfileUser',state: {
        //          firstname: this.props.location.state.firstname,
        //          lastname: this.props.location.state.lastname,
        //          emailid: this.props.location.state.emailid
        //         }
               
        //        }} />;        
         if (this.props.signupdetails && this.props.signupdetails.signupdetails )
        {
            localStorage.username = this.props.signupdetails.signupdetails.username;
            return (
                <div class="minimal theme default login-page marketing standalone" style={{display: 'block'}}>
    <div class="header-bce">
    <div class="container">
        <div class="navbar header navbar-bce">
            <div class="navbar-inner">
                <div class="pull-left">
                            <a href="http://www.homeaway.com/" title="HomeAway" class="logo">
                                <img src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.6/bce/moniker/homeaway_us/logo-bceheader.svg"/>
                            </a>
                </div>
            </div>
        </div>
        <div class="header-bce-birdhouse-container">
            <div class="flip-container">
                <div class="flipper">
                    <div class="front btn-bce">
                        <img src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.6/bce/moniker/homeaway_us/birdhouse-bceheader.svg"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="container" class="container">
        <div id="login-container" class="row">
            <div class="login-header text-center traveler">
                <h1 class="hidden-xs heada">Thank you for creating an account</h1>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="login-form panel panel-dashboard traveler bordeer">
                    <span id="profileUrl" style={{display:'none'}}>/exp/sso/auth?lt=traveler&amp;context=profile</span>
                    <div class="panel-heading">
                        <p class="panel-title text-center">
                            Welcome 
                            <b> {this.props.signupdetails.signupdetails.firstname + this.props.signupdetails.signupdetails.lastname} </b> 
                        </p>
                    </div>
                    <div class="panel-body">
                        <p class="text-center">
                            <img src="https://resources.homeaway.com/resources/29f5da4c/images/anonymous.png"/>
                        </p>
                        <p>
                            Please take a few moments to <a href="/ProfileUser">update your profile</a> with a picture and a few details about yourself. Property owners are more likely to respond more quickly to travelers with profiles.
                        </p>
                    </div>
                    <div class="panel-footer" align="center">
                        <a href="/ProfileUser"><button id="profileButton" type="button" class="btn btn-info btn-lg" onClick='/ProfileUser' value="Update Your Profile" >Update your profile</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
            <div class="row">
                <div class="page-footer text-center col-md-12">
                    <p>Use of this Web site constitutes acceptance of the HomeAway.com <a href="http://www.homeaway.com/travel/site/ha/terms-conditions" target="_blank">Terms and Conditions</a> and <a href="http://www.homeaway.com/travel/site/ha/privacy-policy" target="_blank">Privacy Policy</a>.</p><p>©2018 HomeAway. All rights reserved.</p>
                </div>
            </div>
        </div>
</div>

            ) 
        console.log("signupdeatisl",this.props.signupdetails);
        console.log(this.props.signupdetails.signupdetails.firstname);
        }
   return (
            <div></div>
        );
    }
}

function mapStateToProps(state) {
    return { signupdetails: state.signupdetails };
  }
        
export default connect(mapStateToProps)(SignupSuccess);

