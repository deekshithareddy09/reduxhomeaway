import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {ButtonToolbar,DropdownButton,MenuItem,SplitButton}from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class Dropdownafterlogin extends React.Component {
    constructor (props) {
        super(props);
        console.log(this.props.emailid);
        }
    render() {
      return (
  //        <div>      
  // <ButtonToolbar>
  //   <DropdownButton
  //     title="Default button" id="dropdown-size-medium"
  //   >
  //     <MenuItem eventKey="1">Action</MenuItem>
  //     <MenuItem eventKey="2">Another action</MenuItem>
  //     <MenuItem eventKey="3">Something else here</MenuItem>
  //     <MenuItem divider />
  //     <MenuItem eventKey="4">Separated link</MenuItem>
  //   </DropdownButton>
  // </ButtonToolbar>  
  //        </div>
  <div>
  <ButtonToolbar>
  <DropdownButton
    bsStyle="default"
    title={this.props.emailid}
    noCaret
    id="dropdown-no-caret"
  >
   <LinkContainer to="/Message">
      <MenuItem eventKey={3.1}>Inbox</MenuItem>    
    </LinkContainer> 
    <LinkContainer to="/Tripboardafterclick">
      <MenuItem eventKey={3.1}>My Trips</MenuItem>    
    </LinkContainer> 
    <LinkContainer to="/ProfileUser">
      <MenuItem eventKey={3.1}>My Profile</MenuItem>    
    </LinkContainer> 
    <LinkContainer to="/Account">
      <MenuItem eventKey={3.1}>Account</MenuItem>    
    </LinkContainer> 
    <LinkContainer to="/Dashboardafterlogin">
      <MenuItem eventKey={3.1}>Dashboard</MenuItem>    
    </LinkContainer> 
    <LinkContainer to="/Dashboard">
      <MenuItem eventKey={3.1}>Log Out</MenuItem>    
    </LinkContainer> 
  </DropdownButton>
</ButtonToolbar>
</div>
      );
   }
}
export default Dropdownafterlogin;