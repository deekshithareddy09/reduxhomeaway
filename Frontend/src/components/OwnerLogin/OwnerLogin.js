import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import axios from 'axios';
import {Redirect} from 'react-router';
import { connect } from 'react-redux';
import { logincheck } from "../../actions";
import { OwnerSignup } from "../OwnerSignup/OwnerSignup";


class Ownerlogin extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props);
        this.state = {
            emailid : null,
            password: null,
            switch: false,
            isloggedin: false
        }
        this.handleemailid = this.handleemailid.bind(this);
        this.handlepassword = this.handlepassword.bind(this);
        this.onSubmit= this.onSubmit.bind(this);
    }
    handleemailid = (e) => {
        this.setState({
            emailid : e.target.value
        })
    }
    handlepassword = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const logindata = {
            username : this.state.emailid,
            password : this.state.password
        }
        //set the with credentials to true
        this.props.logincheck(logindata, () => {
            localStorage.username = this.state.emailid;
            console.log("i m in callback");
             this.props.history.push("/OwnerDashboard");
            
        });
    }


    // handleSubmit(e) {
    //     e.preventDefault();

    //     this.setState({ submitted: true });
    //     const { username, password } = this.state;
    //     const { dispatch } = this.props;
    //     if (username && password) {
    //         dispatch(userActions.login(username, password));
    //     }
    // }

    render() {  
        
        return (
            <form name="form" onSubmit={this.onSubmit}>
                <FormGroup align="center">
                    <h1>Log in to HomeAway</h1>
                    <h3>Need an account? <a href="/OwnerSignup" >Sign Up</a></h3>
                </FormGroup>
                <FormGroup align="center">
                    <Panel  className="PanelComp">
                        <Panel.Heading className="PanelHead">Account login</Panel.Heading>
                        <Panel.Body className="pbody">
                            <FormControl className="email"
                            type="email"
                            placeholder="Email Address"
                            onChange={this.handleemailid} />
                            <FormControl className="pwd"
                            type="password"
                            placeholder="Password"
                            onChange={this.handlepassword} />

                        <a href="" id="forgotPasswordUrl">Forgot password?</a>
                            <div align="center" >
                                <Button className="buttonC" type="submit">Login</Button>
                            </div>
                            <Checkbox align="left" defaultChecked="true">
                                keep me signed in
                            </Checkbox>

                            <div className="centered-hr text-center">
                                <span className="text-center"><em>or</em></span>
                            </div>

                            <div className="facebook">
                                <button tabindex="7" className="fbbutton">
                                    <div className="login-button-text">
                                        <span className="logo"><i className="fbicon icon-white pull-left" aria-hidden="true"></i></span>
                                        <span className="fbtext">
                                            Log in with Facebook
                                </span></div>
                                </button>


                                <div className="google">
                                    <button tabindex="8" className="gbutton">
                                        <div className="login-button-text">
                                            <span className="logo-google"><img className="icon-google pull-left" src="//csvcus.homeaway.com/rsrcs/cdn-logos/2.3.2/third-party/google/google-color-g.svg"/>
                                                </span>
                                            <span className="gtext">
                                                    Log in with Google
        </span>
            </div>
        </button>
                                </div>
                            </div>
                    </Panel.Body>
                    </Panel>
                </FormGroup>
            </form>
        );
    }
}


// function mapStateToProps(state) {
//     return { books: state. };
// }


// export { connectedLoginPage as LoginPage }; 
export default connect(null, { logincheck })(Ownerlogin);

