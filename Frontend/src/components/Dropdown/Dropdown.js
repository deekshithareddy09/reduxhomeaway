import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {ButtonToolbar,DropdownButton,MenuItem,SplitButton}from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class Dropdown extends React.Component {
   render() {
      return (
  //        <div>      
  // <ButtonToolbar>
  //   <DropdownButton
  //     title="Default button" id="dropdown-size-medium"
  //   >
  //     <MenuItem eventKey="1">Action</MenuItem>
  //     <MenuItem eventKey="2">Another action</MenuItem>
  //     <MenuItem eventKey="3">Something else here</MenuItem>
  //     <MenuItem divider />
  //     <MenuItem eventKey="4">Separated link</MenuItem>
  //   </DropdownButton>
  // </ButtonToolbar>  
  //        </div>
  <div>
  <ButtonToolbar>
  <DropdownButton
    bsStyle="default"
    title="Login"
    noCaret
    id="dropdown-no-caret"
  >
   <LinkContainer to="/Travelerlogin">
      <MenuItem eventKey={3.1}>Traveler Login</MenuItem>    
    </LinkContainer>
    <LinkContainer to="/OwnerLogin">
      <MenuItem eventKey={3.1}>Owner Login</MenuItem>    
    </LinkContainer> 
    
  </DropdownButton>
</ButtonToolbar>
</div>
      );
   }
}
export default Dropdown;