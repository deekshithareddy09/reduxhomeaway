import React, {Component} from 'react';
import './Signup.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';
import { signupCheck } from "../../actions";
import $ from 'jquery';


//import axios from 'axios';
//import cookie from 'react-cookies';
//import {Redirect} from 'react-router';

class Signup extends Component{
    constructor(props){
        super(props);
        this.state = {
            test: false,
            firstname : null,
            lastname : null,
            username : null,
            password : null,
            switch: false
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handlebuttonclick = this.handlebuttonclick.bind(this);
        this.handlefirstname = this.handlefirstname.bind(this);
        this.handlelastname = this.handlelastname.bind(this);
        this.handleusername = this.handleusername.bind(this);
        this.handlepassword = this.handlepassword.bind(this);

    }

    handlefirstname = (e) => {
        this.setState({
            firstname : e.target.value
        })
    }

    handlelastname = (e) => {
        this.setState({
            lastname : e.target.value
        })
    }

    handleusername = (e) => {
        this.setState({
            username : e.target.value
        })
    }

    handlepassword = (e) => {
     this.setState({
            password : e.target.value
     })
    }
    handlebuttonclick = (e) => {
        this.setState({
            test: true
        })
    }

    onSubmit = (e) => {
        var headers = new Headers();
        //prevent page from refresh
        e.preventDefault();
        const signupdata = {
            firstname : this.state.firstname,
            lastname : this.state.lastname,
            username : this.state.username,
            password : this.state.password,
            type : "traveler"
        }
        console.log("signupdta".signupdata);
        //set the with credentials to true
        this.props.signupCheck(signupdata, () => {
            console.log("i m in  signup callback");
            this.props.history.push("/SignupSuccess");
        });
    }

    render(){
        return(
<div id="root">
<div class="header-bce">
    <div class="container">
        <div class="navbar header navbar-bce">
            <div class="navbar-inner">
                <div class="pull-left">
                            <a href="http://www.homeaway.com/" title="HomeAway" class="logo" alt="image">
                                <img src='https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.6/bce/moniker/homeaway_us/logo-bceheader.svg'/>
                            </a>
                </div>
            </div>
        </div>
        <div class="header-bce-birdhouse-container">
            <div class="flip-container">
                <div class="flipper">
                    <div class="front btn-bce">
                        <img src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.6/bce/moniker/homeaway_us/birdhouse-bceheader.svg"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <input type="hidden" id="displayMarketing" value="false"/>
 <div id="container" class="container">
 <div id="login-container" class="row">
 <div class="login-header text-center col-md-12">
 <h1 class="hidden-xs">Sign up for HomeAway</h1>
 <div class="footer-top traveler"></div>
 <div class="footer traveler" align="center">
<div><span>Already have an account?</span><a class="linkstyle" id="sign-in-link" href="/Travelerlogin">Log in</a></div>
</div>
</div>
<div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
<div class="panel panel-dashboard">
<div class="login-wrapper">
<div class="login-form">
<fieldset id="login-form-fieldset"/>
<div class="panel-body">
     <form id="login-form" name="fm1" class="singleSubmit" action="" onsubmit="return false"  method="post"> 
        <div id="createAccountFields" style={{ display: this.state.test ? 'block' : 'none'}} >
            <div id="messages">
            </div>
            <div class="form-group ">
                <div class="name name-registration">
                    <label for="firstName" >First name</label>
                    <input id="firstName" name="firstname" class="form-control input-lg"  onChange={this.handlefirstname} tabindex="1" Placeholder="First Name" type="text"  size="20" autocomplete="on"/>
                 </div> 
                 <div class="name name-registration" > 
                    <label for="lastName" >Last Name</label>
                    <input id="lastName" name="lastname"  onChange={this.handlelastname} class="form-control input-lg lastname" tabindex="1" Placeholder="Last Name" type="text"  size="20" autocomplete="on"/>
                </div> 
            </div>
            <div class="form-group">
                <label for="emailAddress" >Email Address</label>
                <input id="emailAddress" type="email" name="emailAddress" onChange={this.handleusername} class="form-control input-lg" tabindex="3" Placeholder="Email address" type="email"  size="20" autocomplete="on"/>
            </div>
            <div class="form-group">
                <label for="password" >Password</label>
                <input id="password" name="password" onChange={this.handlepassword} class="form-control input-lg" tabindex="4" Placeholder="Password" type="password"  size="20" autocomplete="off"/>
            </div>
            
            <input tabindex="6" type="submit" class="btn btn-primary btn-lg btn-block btn-cas-primary"
                   value="Sign Me Up" id="form-submit"  onClick={this.onSubmit} />
        </div>
     </form> 
        <div class="signupbutton" style={{ display: this.state.test ? 'none' : 'block'}}>
        <button type="button" tabindex="6" class="btn btn-primary btn-lg btn-block btn-cas-primary"   id="emailRegisterButton" onClick={this.handlebuttonclick} >
            Sign up with Email
        </button>
        </div>
    <div class="centered-hr text-center">
        <span class="text-center"><em>or</em></span>
    </div>
    <div class="facebook">
        <button tabindex="7" class="third-party-login-button fb-button">
            <div class="login-button-text">
                <span class="logo"><i class="icon-facebook icon-white pull-left" aria-hidden="true"></i></span>
                <span class="text text-center pull-right">
                    Log in with Facebook
                </span>
            </div>
        </button>
    </div>
    <div class="google">
        <button tabindex="8" class="third-party-login-button google-button">
            <div class="login-button-text">
                {/* <span class="logo-google"><img class="icon-google pull-left" src="//csvcus.homeaway.com/rsrcs/cdn-logos/2.3.2/third-party/google/google-color-g.svg"/></span> */}
                <span class="text text-center pull-right">
                    Log in with Google</span>
            </div>
        </button>
    </div>
	<p id="fb-p" class="facebook text-center traveler"><small>We don't post anything without your permission.</small></p>
	<div class="form-group">
        <div class="text-center">
        <label class="textstyle">By creating an account you are accepting our <a class="linkstyle" href="http://www.homeaway.com/info/about-us/legal/terms-conditions" target="_blank">Terms and Conditions</a> and <a class="linkstyle" href="http://www.homeaway.com/info/about-us/legal/privacy-policy" target="_blank">Privacy Policy</a></label>
        </div>
    </div>
	</div>
	</div>
	</div>
	</div>
    </div>
    </div>
    </div>

		<div class="container1">
            <div class="row">
                <div class="page-footer text-center col-md-12">
                    <p>Use of this Web site constitutes acceptance of the HomeAway.com <a class="linkstyle" href="http://www.homeaway.com/travel/site/ha/terms-conditions" target="_blank">Terms and Conditions</a> and <a class="linkstyle" href="http://www.homeaway.com/travel/site/ha/privacy-policy" target="_blank">Privacy Policy</a></p><p>©2018 HomeAway. All rights reserved.</p>
                </div>
            </div>
        </div>
</div>
        )

    }
    }


            
    export default connect(null, { signupCheck })(Signup);
