
// import React from 'react';
// import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
// import axios from 'axios';
// import {Redirect} from 'react-router';
// import { Link } from "react-router-dom";
// import { connect } from "react-redux";
//     class PaginationTest1 extends React.Component {
//       constructor() {
//         console.log(this.props.result);
//         super();
//         this.state = {
//           todos: ['a','b','c','d','e','f','g','h','i','j','k'],
//           currentPage: 1,
//           todosPerPage: 3
//         };
//         this.handleClick = this.handleClick.bind(this);
//       }

//       handleClick(event) {
//         this.setState({
//           currentPage: Number(event.target.id)
//         });
//       }

//       render() {
//         const { todos, currentPage, todosPerPage } = this.state;

//         // Logic for displaying current todos
//         const indexOfLastTodo = currentPage * todosPerPage;
//         const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
//         const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

//         const renderTodos = currentTodos.map((todo, index) => {
//           return <li key={index}>{todo}</li>;
//         });

//         // Logic for displaying page numbers
//         const pageNumbers = [];
//         for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
//           pageNumbers.push(i);
//         }

//         const renderPageNumbers = pageNumbers.map(number => {
//           return (
//             <li
//               key={number}
//               id={number}
//               onClick={this.handleClick}
//             >
//               {number}
//             </li>
//           );
//         });

//         return (
//           <div>
//             <ul>
//               {renderTodos}
//             </ul>
//             <ul id="page-numbers">
//               {renderPageNumbers}
//             </ul>
//           </div>
//         );
//       }
//     }


//     export default connect(null)(PaginationTest1);
