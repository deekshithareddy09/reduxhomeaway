import React from 'react';
import { Pager,Pagination} from 'react-bootstrap';

let active = 7;
let items = [];
for (let number = 1; number <= 10; number++) {
  items.push(
    <Pagination.Item active={number === active}>{number}</Pagination.Item>
  );
}

const Paginationbasic = (
  <div>
    <Pagination bsSize="large">{items}</Pagination>
    <br />

    <Pagination bsSize="medium">{items}</Pagination>
    <br />

    <Pagination bsSize="small">{items}</Pagination>
  </div>
);

class PaginationTest extends React.Component {
    render() {
        return (<div>
            <h1>hi hello</h1>
            <Pager>
  <Pager.Item previous href="#">
    &larr; Previous Page
  </Pager.Item>
  <Pager.Item disabled next href="#">
    Next Page &rarr;
  </Pager.Item>
</Pager>

{Paginationbasic}
        </div>
    );
    }
}
export default PaginationTest;