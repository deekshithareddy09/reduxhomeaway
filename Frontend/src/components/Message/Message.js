import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import DashboardHeader from '../Dashboard/DashboardHeader';

class Message extends Component {
    constructor(props){
        super(props);
        this.state={
            show: false
        }
    }

  //Define component that you wanbt to render
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }
  /*Action call
  Whenever onSubmit event is triggered, execute an action call called createBook 
  */
  onSubmit(values) {
      this.setState({
          show:true
      })
      
}

  render() {
    const { handleSubmit } = this.props;

    return (

      //handleSubmit is the method that comes from redux and it tells redux what to do with the submitted form data
      //Field is a component of redux that does the wiring of inputs to the redux store.
      <div>
      <DashboardHeader />
      <div class="col-xs-6 col-sm-8 content-panel-container">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div>
                        <div class="checklist-header-container ">
                           <h3><span>Support to contactowner</span></h3>
                           <hr/>
                        </div>
                        <div>
                           <div></div>

     <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
      
        <Field
          label="QUestion"
          name="question"
          component={this.renderField}
        />

        <Field
          label="Description"
          name="description"
          component={this.renderField}
        />
        <Field
          label="type"
          name="messagetype"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Send Message</button>
        <h1 style={{ display: this.state.show ? 'block' : 'none'}}>Message successfully sent</h1>
      </form>
      </div>
                     </div>
                     <hr/>
                  </div>
               </div>
               </div>
      </div>
    );
  }
}

function validate(values) {

  const errors = {};

  // Validate the inputs from 'values'
  if (!values.address) {
    errors.address = "Enter an address";
  }
  if (!values.city) {
    errors.city = "Enter city";
  }
  if (!values.state) {
    errors.state = "Enter state";
  }

  // If errors is empty, the form is fine to submit
  // If errors has *any* properties, redux form assumes form is invalid
  return errors;
}

export default reduxForm({
  validate,
  form: "new",
})(Message);
