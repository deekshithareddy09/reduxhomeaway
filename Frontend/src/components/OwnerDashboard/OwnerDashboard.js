
import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import axios from 'axios';
import './OwnerDashboard.css';
import {Redirect} from 'react-router';
import CCtest from '../CCtest/CCtest';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocation } from "../../actions";
import HeaderOwnerDashboard from '../Photo/HeaderOwnerDashboard';
import PaginationTest1 from '../Pagination/PaginationTest1';
var name= localStorage.owneremailid;
class OwnerDashboard extends React.Component {
    constructor(props){
    super(props);
    this.state = {
        todos: [],
        travelerdata: [],
        currentPage: 1,
        todosPerPage: 2
    }
    this.handleClick = this.handleClick.bind(this);
    this.handlenumberClick = this.handlenumberClick.bind(this);
}

handlenumberClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

handleClick(e){
    e.preventDefault();
    // propid: null
    // console.log("propid "+propid);
    console.log("event",e.currentTarget.getAttribute('data-column'));
    const travelerbooked=e.currentTarget.getAttribute('data-column');
    const data = {
        travelerbooked : e.currentTarget.getAttribute('data-column')
    }
    axios.post('/travelerbooking',data)
                   .then((response) => {
                       console.log("response",response.data);
                    this.setState({
                    travelerdata: this.state.travelerdata.concat(response.data)
                    })
                    console.log("travelerdata",this.state.travelerdata);
               });
        }
componentDidMount(){
     console.log("cd   "+localStorage.owneremailid);
     const ownerusername = {
         username : localStorage.username
     }
    axios.post('/ownerdashboard',ownerusername)
            .then((response) => {
                console.log(response.data);
            this.setState({
                todos : this.state.todos.concat(response.data) 
            });
            console.log(this.state.todos);
        });
}
render() {    
    const { todos, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    const indexOfLastTodo = currentPage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);
    const renderTodos = currentTodos.map((todo, index) => {
        return(
            <div> 
            <table class=" col-xs-6 col-sm-8 list-table listingTable table table-hover">
            <div className="carouselowner">
                 <CCtest propertyid= {todo.propid} /> 
                </div>
       <tbody>
          <tr class="INCOMPLETE">
             <td class="listing-summary" data-listingguid="121.7352502.6707776" data-partnerfeedenabled="true">
                <div class="property-combine">
                   <div class="alerts-summary">
                      <span data-automation-id="LISTING_INCOMPLETE" class="alert-inner text-warning">
                      </span>
                   </div>
                   <div class="row-fluid">
                      <div class="span2">
                         <a href="/pxe/feed/121.7352502.6707776" class="thumbnail-link">
                         </a>
                      </div>
                      <div class="span8">
                         <div class="property-details">
                            <h3>
                                Property  {todo.headline}
                            </h3>
                            <div class="address">
                            {todo.address+ todo.city + todo.state}<br/>
                            </div>
                            <span class="listing-ids">
                            {/* HomeAway.com {todo.id} */}
                            </span>
                            {/* <span class="inline-rating-container" id="inline-rating-container-listing-121_7352502_6707776" data-listingid="121.7352502.6707776">
                            <a href="/gd/dash/reviews/121.7352502.6707776"><span class="rating rating-0"></span></a></span> */}
                            <button type="button" class="btn btn-secondary btn-sm" data-column={todo.travelerbooked} onClick={this.handleClick} >booking History</button>
                            <p>Booked by {this.state.travelerdata.firstname}</p>
                         </div>
                      </div>
                   </div>
                </div>
             </td>
          </tr>
             </tbody>
    </table>
    </div>
            
        ) 
      });
  



const pageNumbers = [];
for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
  pageNumbers.push(i);
}

const renderPageNumbers = pageNumbers.map(number => {
    return (
      <li
        key={number}
        id={number}
        onClick={this.handlenumberClick}
      >
        {number}
      </li>
    );
  });

  return (
    <div>
   <div>
    <HeaderOwnerDashboard/>   
    {renderTodos}
   </div>
       <div id ="footer">
       <footer>
       <ul id="page-numbers">
       {renderPageNumbers}
       </ul>
       </footer></div>
            </div>
    );
  }   
}
export default connect(null)(OwnerDashboard);
























