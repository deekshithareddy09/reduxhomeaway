
import React from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Panel, Button, Checkbox } from 'react-bootstrap';
import axios from 'axios';
import '../Dashboard/Dashboard.css';
import {Redirect} from 'react-router';
import { connect } from 'react-redux';
import CCtest from '../CCtest/CCtest';
import DashboardHeader from '../Dashboard/DashboardHeader';

class Tripboardafterclick extends React.Component {
    constructor(props){
    super(props);
    console.log(localStorage.username);
    this.state = {
        todos : [],
        currentPage: 1,
        todosPerPage: 2
    }
    this.handlenumberClick = this.handlenumberClick.bind(this);

}
componentDidMount(){
    const data = {
            username : localStorage.username
                //gotopropertysearch: this.state.gotopropertysearch
    }
    axios.post('/tripboardafterclick',data)
            .then((response) => {
            this.setState({
                todos : this.state.todos.concat(response.data) 
            });
            console.log("travelr data",this.state.travelerdata);
        });
}
 
handlenumberClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  } 
render() {    
    const { todos, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    const indexOfLastTodo = currentPage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);
    const renderTodos = currentTodos.map((todo, index) => {
    
        return(
            <div>
            <div className="carouselowner">
                 <CCtest propertyid= {todo.propid} /> 
                </div> 
            <p>{todo.propid}</p>
         </div>
            
        )
    })
   
        

const pageNumbers = [];
for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
  pageNumbers.push(i);
}




const renderPageNumbers = pageNumbers.map(number => {
    return (
      <li
        key={number}
        id={number}
        onClick={this.handlenumberClick}
      >
        {number}
      </li>
    );
  });
        return(
     <div>
     <div>
     <DashboardHeader />
      {renderTodos}
     </div>
         <div id ="footer">
         <footer>
         <ul id="page-numbers">
         {renderPageNumbers}
         </ul>
         </footer></div>
              </div>
     
        );
    }
}
export default connect(null)(Tripboardafterclick);
