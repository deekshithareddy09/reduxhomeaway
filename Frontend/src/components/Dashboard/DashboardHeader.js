import React, {Component} from 'react';
import './Dashboard.css';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import Dropdown from '../Dropdown/Dropdown.js';
import Dropdownafterlogin from '../Dropdownafterlogin/Dropdownafterlogin';
import Tripboard from '../Tripboard/Tripboard';
//import Dashboardafterlogin from '../Dropdownafterlogin/Dropdownafterlogin';
import {Redirect} from 'react-router';
import '../Photo/Photo.css';
import axios from 'axios';
import { searchProperty } from "../../actions";
import { connect } from 'react-redux';


//import axios from 'axios';
//import cookie from 'react-cookies';
//import {Redirect} from 'react-router';



class DashboardHeader extends Component{
    constructor (props) {
        super(props);
        this.state = {
            testfamily: false
                  };  
          this.handlefamilyClick = this.handlefamilyClick.bind(this);
        }
         
        handlefamilyClick = (e) => {
          this.setState({
            testfamily: true
          })
        };  
    render(){
        return(
  <div id="root">
<div class="site-header site-header--inverse border-border-color homeaway_us">
  <a class="site-header__tray-toggle hidden-md hidden-lg" role="button" tabindex="0">
  <span class="SVGIcon SVGIcon--24px">
  <svg data-id="SVG_MENU__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
  <path d="M21 5H3M21 12H3M21 19H3"></path></g>
  </svg>
  </span>
  </a>
  <div class="site-header-logo">
  <a class="site-header-logo__link flex-item" href="https://www.homeaway.com/" title="HomeAway.com">
  <img alt="HomeAway logo" class="site-header-logo__img img-responsive" role="presentation" src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.11.0/bce/moniker/homeaway_us/logo-bceheader-white.svg"/>
  </a>
  </div>
  <div class="site-header__flex-spacer"></div>
  <div class="site-header-nav" role="navigation">
  <Tripboard/>                  
    <img alt="avatar" aria-hidden="true" class="site-header-nav__avatar img-circle" src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.3/bce/brand/misc/default-profile-pic.png"></img>

  {/* <Dropdownafterlogin emailid={this.props.location.state.emailid}/>  */}
  <Dropdownafterlogin />
  </div>
  <a class="site-header-list-your-property btn btn-default btn-inverse" data-bypass="true" href="/Ownerlogin" >List your property</a>
  <div class="site-header-birdhouse" onClick={this.handlefamilyClick}>
  <div class="site-header-birdhouse__toggle" data-toggle="dropdown" role="button" tabindex="0">
  <img alt="HomeAway birdhouse" class="site-header-birdhouse__image" role="presentation" src="//csvcus.homeaway.com/rsrcs/cdn-logos/2.11.0/bce/moniker/homeaway_us/birdhouse-bceheader-white.svg"/>
  <div class="site-header-birdhouse__tagline">Our<br/>Family of<br/> Brands</div>
  </div>
  <div class="site-header-birdhouse__dropdown-menu dropdown-menu fade" style={{ display: this.state.testfamily ? 'block' : 'none'}} >
  <p>HomeAway is the world leader in vacation rentals. We offer the largest selection of properties for any travel occasion and every budget. We're committed to helping families and friends find a perfect vacation rental to create unforgettable travel experiences together.</p>
  <div><a href="https://www.homeaway.com/info/homeaway/about-the-family">Learn more</a></div>
  <div class="text-right site-about-logo-wrapper">
  <img alt="logo" src="//csvcus.homeaway.com/rsrcs/cdn-logos/1.5.1/bce/brand/homeaway/logo-simple.svg"/>
  </div></div></div>
  
  </div>


    </div>
        )
    }
}



export default connect(null)(DashboardHeader);

























