import React, {Component} from 'react';
import './Dashboard.css';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import Dropdown from '../Dropdown/Dropdown.js';
import Dropdownafterlogin from '../Dropdownafterlogin/Dropdownafterlogin';
import Tripboard from '../Tripboard/Tripboard';
//import Dashboardafterlogin from '../Dropdownafterlogin/Dropdownafterlogin';
import {Redirect} from 'react-router';
import '../Photo/Photo.css';
import axios from 'axios';
import { searchProperty } from "../../actions";
import { connect } from 'react-redux';


//import axios from 'axios';
//import cookie from 'react-cookies';
//import {Redirect} from 'react-router';



class Dashboardafterlogin extends Component{
    constructor (props) {
        super(props);
        console.log("in dashboiard",localStorage.username);
        console.log(this.props);
        this.state = {
          startDate: moment(),
          endDate: moment(),
          location: null,
          resultdata: null,
          gotopropertysearch: false,
          testfamily: false,
          isloggedin: false
        };  
        this.handlestartDateChange = this.handlestartDateChange.bind(this);
        this.handleendDateChange = this.handleendDateChange.bind(this);
        this.handlesearchclick = this.handlesearchclick.bind(this);
        this.handlelocationChange = this.handlelocationChange.bind(this);
      }
      handlelocationChange = (e) => {
        this.setState({
          location: e.target.value
        });
      }
    //  handleloginCheck() {
    //   this.setState({
    //     isloggedin: this.props.location.state.isloggedin
    //   });
    //  }
      handlestartDateChange(date) {
        this.setState({
          startDate: date
        });
      }



      handleendDateChange(date) {
        this.setState({
          endDate: date
        });
      }

      handlefamilyClick = (e) => {
        this.setState({
          testfamily: true
        });
      }

      handlesearchclick = (e) => {
        var headers = new Headers();
        //prevent page from refresh
        e.preventDefault();
        const data = {
            startDate : this.state.startDate,
            endDate : this.state.endDate,
            location : this.state.location,
            username : localStorage.username
            //gotopropertysearch: this.state.gotopropertysearch
        }
        axios.defaults.withCredentials = true;
        //make a post request with the user data
        axios.post('/dashboardsearch',data)
            .then(response => {
                console.log("Status Code : ",response.status);
                if(response.status === 200){
                    console.log("response data "+response.data);
                    // this.setState({
                    //   gotopropertysearch: true
                    // });
                    this.setState({
                      //gotopropertysearch: true
                      resultdata: response.data
                    });
                    console.log(this.state.resultdata);
                    //console.log(this.state.gotopropertysearch);
                }else{
                    console.log("error in getting the response ");
                }
            });   
    }
    
    render(){
      if(this.state.resultdata!=undefined){
        return <Redirect to={{pathname: '/Property',state: { resultdata: this.state.resultdata}
        }} />;}
        return(
  <div id="root">
  <header role="banner">
  <div class="HeroImage" >
  <div class="HeroImage__content">
  <div class="downcontent">
  <div class="site-header site-header--inverse border-border-color homeaway_us">
  <a class="site-header__tray-toggle hidden-md hidden-lg" role="button" tabindex="0">
  <span class="SVGIcon SVGIcon--24px">
  <svg data-id="SVG_MENU__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
  <path d="M21 5H3M21 12H3M21 19H3"></path></g>
  </svg>
  </span>
  </a>
  <div class="site-header-logo">
  <a class="site-header-logo__link flex-item" href="https://www.homeaway.com/" title="HomeAway.com">
  <img alt="HomeAway logo" class="site-header-logo__img img-responsive" role="presentation" src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.11.0/bce/moniker/homeaway_us/logo-bceheader-white.svg"/>
  </a>
  </div>
  <div class="site-header__flex-spacer"></div>
  <div class="site-header-nav" role="navigation">
  <Tripboard/>                  
    <img alt="avatar" aria-hidden="true" class="site-header-nav__avatar img-circle" src="https://csvcus.homeaway.com/rsrcs/cdn-logos/2.10.3/bce/brand/misc/default-profile-pic.png"></img>

  {/* <Dropdownafterlogin emailid={this.props.location.state.emailid}/>  */}
  <Dropdownafterlogin />
  </div>
  <a class="site-header-list-your-property btn btn-default btn-inverse" data-bypass="true" href="Ownerlogin" >List your property</a>
  <div class="site-header-birdhouse" onClick={this.handlefamilyClick}>
  <div class="site-header-birdhouse__toggle" data-toggle="dropdown" role="button" tabindex="0">
  <img alt="HomeAway birdhouse" class="site-header-birdhouse__image" role="presentation" src="//csvcus.homeaway.com/rsrcs/cdn-logos/2.11.0/bce/moniker/homeaway_us/birdhouse-bceheader-white.svg"/>
  <div class="site-header-birdhouse__tagline">Our<br/>Family of<br/> Brands</div>
  </div>
  <div class="site-header-birdhouse__dropdown-menu dropdown-menu fade" style={{ display: this.state.testfamily ? 'block' : 'none'}} >
  <p>HomeAway is the world leader in vacation rentals. We offer the largest selection of properties for any travel occasion and every budget. We're committed to helping families and friends find a perfect vacation rental to create unforgettable travel experiences together.</p>
  <div><a href="https://www.homeaway.com/info/homeaway/about-the-family">Learn more</a></div>
  <div class="text-right site-about-logo-wrapper">
  <img alt="logo" src="//csvcus.homeaway.com/rsrcs/cdn-logos/1.5.1/bce/brand/homeaway/logo-simple.svg"/>
  </div></div></div>
  
  </div>
  </div>
  </div>
  <div class="Jumbotron">
  <div class="Jumbotron__wrapper">
  <div class="Jumbotron__content">
  <h1 class="HeadLine">
  <span class="HeadLine__text">Book beach houses, cabins,</span>
  <span class="HeadLine__text">condos and more, worldwide</span>
  </h1>
  <div class="search__container">
  <div class="search__cta-container--hidden">
  <button class="search__cta"><span class="search__cta-label">Where do you want to go?</span> }
  <span class="btn btn-primary search__cta-icon"><svg class="search__icon" width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
  </svg></span></button></div><div class="search__box-container"><div></div>
  <div class="context context--xs-show">
  <div class="search__box consolidated-searchbox">
  <form class="SearchForm checkin-focus"><div role="presentation" class="react-destination-typeahead closed">
  <div class="search-destination-input"><div class="sr-only"><span role="status" aria-live="polite" class="sr-only"></span></div>
  <div role="presentation" class="search-destination-input__header state--closed">
  <div class="search-destination-input__header__inputs form-group has-feedback floating-label has-icon" data-toggle="label">
  <input type="text" id="react-destination-typeahead" size="10" placeholder="Where do you want to go?" onChange={this.handlelocationChange} class="control form-control"  autocomplete="on" role="combobox" aria-haspopup="listbox" aria-controls="Typeahead__results" aria-autocomplete="both" aria-expanded="true"/>
  <span class="form-control-icon search-destination-input__icon">
  <span class="SVGIcon SVGIcon--24px"><svg data-id="SVG_SEARCH__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><circle cx="10.5" cy="10.5" r="7.5"></circle><path d="M16 16l5 5"></path></g>
  </svg></span></span></div></div></div></div>
  <div class="DatePicker__range">
  <div class="date-range form-combined form-combined--dates">
  <span  class="datepickerspan"  role="button" tabindex="0">
  <div class="form-combined-row">
  <div class="form-group position-relative form-combined__input1">
  <div class="form-group floating-label has-icon">
  <DatePicker   className="height" selected={this.state.startDate}
  onSelect={this.handleSelect} 
  onChange={this.handlestartDateChange} /> 
 <span class="form-control-icon">
  <svg class="date-picker__icon" width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <g id="Family-Segment" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
  <g id="Icon/Calendar/24---Gray" transform="translate(-2.000000, -2.000000)" stroke="#323F4D" stroke-width="1.5">
  <rect id="Rectangle-6" x="3" y="5" width="18" height="16" rx="2.5"></rect>
  <path d="M7,3 L7,5" id="Line"></path><path d="M3,9 L21,9" id="Line"></path><path d="M17,3 L17,5" id="Line"></path></g></g></svg></span>
  </div>
  
  <div class="picker__overlay picker__overlay--selectable"></div></div><div class="form-group form-combined__svg">
  <span class="SVGIcon SVGIcon--24px">
  <svg data-id="SVG_ARROW_RIGHT__24" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <path d="M14 18l6-6-6-6M4 12h16" fill="none" stroke-linecap="round" stroke-linejoin="round"></path>
  </svg>
  </span>
  </div>
  <div class="form-group position-relative form-combined__input2">
  <div class="form-group floating-label has-icon">
  <DatePicker  className="height" selected={this.state.endDate}
  onSelect={this.handleSelect} 
  onChange={this.handleendDateChange} /> 
  <span class="form-control-icon">
  <svg class="date-picker__icon" width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <g id="Family-Segment" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
  <g id="Icon/Calendar/24---Gray" transform="translate(-2.000000, -2.000000)" stroke="#323F4D" stroke-width="1.5">
  <rect id="Rectangle-6" x="3" y="5" width="18" height="16" rx="2.5"></rect>
  <path d="M7,3 L7,5" id="Line"></path><path d="M3,9 L21,9" id="Line"></path><path d="M17,3 L17,5" id="Line"></path>
  </g></g></svg>
  </span>
  </div>
  
  <div class="picker__overlay picker__overlay--selectable"></div>
  </div>
  </div></span></div></div>
  <div class="guest-picker">
  <span class="guest-pickerspan"  role="button" tabindex="0">
  <div class="position-relative">
  <div class="form-group floating-label has-icon"><label class="has-icon" for="">Guests</label>
  <input type="text" class="form-control" aria-label="Guests" name="Guests"  data-wdio="guest-picker-input"/>
  <span class="form-control-icon">
  <svg class="guest-picker__icon" width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <g id="Family-Segment" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
  <g id="Icon/Person/24---Gray" transform="translate(-2.000000, -2.000000)" fill-rule="nonzero" stroke="#323F4D" stroke-width="1.5">
  <path d="M12,12 C9.8,12 8,10.2 8,8 L8,7 C8,4.8 9.8,3 12,3 C14.2,3 16,4.8 16,7 L16,8 C16,10.2 14.2,12 12,12 Z" id="Shape"></path>
  <path d="M21,21 C21,17.64 18.1714286,15 14.5714286,15 L9.42857143,15 C5.82857143,15 3,17.64 3,21" id="Shape"></path>
  </g></g></svg></span></div>
  <div class="picker__overlay picker__overlay--selectable"></div>
  </div></span></div>
  <div class="search__button-container">
  <button type="submit" onClick={this.handlesearchclick} class="SearchForm__button search__button btn btn-primary">Search</button>
  </div></form></div></div></div></div></div>
  <div class="ValueProps hidden-xs">
  <ul class="ValueProps__list">
  <li class="ValueProps__item"><strong class="ValueProps__title">Your whole vacation starts here</strong>
  <span class="ValueProps__blurb">Choose a rental from the world's best selection</span></li>
  <li class="ValueProps__item"><strong class="ValueProps__title">Book and stay with confidence</strong>
  <span class="ValueProps__blurb">
  <a href="https://www.homeaway.com/info/ha-guarantee/travel-with-confidence?icid=il_o_link_bwc_homepage">Secure payments, peace of mind</a>
  </span></li>
  <li class="ValueProps__item"><strong class="ValueProps__title">Your vacation your way</strong><span class="ValueProps__blurb">More space, more privacy, no compromises</span>
  </li>
  </ul>
  </div></div></div></div>
  </header>
  </div>
  
        )
    }
}



export default connect(null)(Dashboardafterlogin);

