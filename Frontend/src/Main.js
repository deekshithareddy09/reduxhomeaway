import React, {Component} from 'react';
// import {Route} from 'react-router-dom';
import Dashboard from './components/Dashboard/Dashboard';
import Signup from './components/Signup/Signup';
import Message from './components/Message/Message';
import Travelerlogin from './components/Travelerlogin/Travelerlogin';
import Propertysearch from './components/Propertysearch/Propertysearch';
import Navbar from './components/Navbar/Navbar';
import SignupSuccess from './components/SignupSuccess/SignupSuccess';
import Dropdown from './components/Dropdown/Dropdown';
import OwnerLogin from './components/OwnerLogin/OwnerLogin';
import Dropdownafterlogin from './components/Dropdownafterlogin/Dropdownafterlogin';
import Dashboardafterlogin from './components/Dashboard/Dashboardafterlogin';
import Detailsform from './components/Details/Detailsform';
import Photo from './components/Photo/Photo';
import HeaderOwnerDashboard from './components/Photo/HeaderOwnerDashboard';
import OwnerSignup from './components/OwnerSignup/OwnerSignup';
import Availability from './components/Availability/Availability';
import Pricingform from './components/Pricing/Pricingform';
import Property from './components/Property/Property';
import Tripboardafterclick from './components/Tripboardafterclick/Tripboardafterclick';
import DropdownafterDashboard from './components/Dropdown/DropdownafterDashboard';
import OwnerDashboard from './components/OwnerDashboard/OwnerDashboard';
import CCtest from './components/CCtest/CCtest';
import ProfileUser from './components/ProfileUser/ProfileUser';
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import promise from "redux-promise";
import RootReducer from "./reducers";
import Locationform from './components/Location/Locationform';
import Bookingform from './components/Booking/Bookingform';
import PaginationTest from './components/Pagination/PaginationTest';
import DashboardHeader from './components/Dashboard/DashboardHeader';

//middleware settings
// To resolve promise to store we use apply
const composePlugin = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const createStoreWithMiddleware = applyMiddleware(promise)(createStore);
const store = createStore(RootReducer, composePlugin(applyMiddleware(promise)));
//import Booking from './Booking/Booking';
class Main extends Component {
    render(){
        return(
            <Provider store={store}>
            <BrowserRouter>
            <div>
                <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route path="/Bookingform" component={Bookingform}/>     
                <Route path="/Detailsform" component={Detailsform}/>  
                <Route path="/Locationform" component={Locationform}/>
                <Route path="/Travelerlogin" component={Travelerlogin}/>
                <Route path="/PaginationTest" component={PaginationTest}/> 
                <Route path="/Message" component={Message}/>         
                {/* /*Render Different Component based on Route */}
                <Route path="/Dashboard" component={Dashboard}/>
                <Route path="/ProfileUser" component={ProfileUser}/>
                <Route path="/HeaderOwnerDashboard" component={HeaderOwnerDashboard}/>
                <Route path="/Signup" component={Signup}/>
                <Route path="/OwnerDashboard" component={OwnerDashboard}/>
                <Route path="/OwnerSignup" component={OwnerSignup}/>
                <Route path="/Travelerlogin" component={Travelerlogin}/>
                <Route path="/Propertysearch" component={Propertysearch}/>
                <Route path="/SignupSuccess" component={SignupSuccess}/>
                <Route path="/Dropdown" component={Dropdown}/>
                <Route path="/Dropdownafterlogin" component={Dropdownafterlogin}/>
                <Route path="/OwnerLogin" component={OwnerLogin}/>      
                <Route path="/Photo" component={Photo}/>
                <Route path="/Dashboardafterlogin" component={Dashboardafterlogin}/>
                <Route path="/Property" component={Property}/>
                <Route path="/Availability" component={Availability}/>
                <Route path="/Tripboardafterclick" component={Tripboardafterclick}/>
                <Route path="/CCtest" component={CCtest}/>
                <Route path="/DropdownafterDashboard" component={DropdownafterDashboard}/>
                <Route path="/Pricingform" component={Pricingform}/>
                <Route path="/DashboardHeader" component={DashboardHeader}/>


           
            </Switch>
            </div>
            </BrowserRouter>
            </Provider>
            );
        }
    }




export default Main;


