import { combineReducers } from "redux";
import SignupReducer from "./reducer_books";
import LoginReducer from "./reducer_books";
import Reducer from "./reducer_books";
import { reducer as formReducer } from "redux-form";
import SearchReducer from "./reducer_books";
import ProfileReducer from "./reducer_books";
import PostPropertyReducer from "./reducer_books";

const rootReducer = combineReducers({
  signupdetails: SignupReducer,
  loginstatus: LoginReducer,
  form: formReducer,
  searchdetails: SearchReducer,
  profiledetails: ProfileReducer,
  propertydetails: PostPropertyReducer
});

export default rootReducer;
