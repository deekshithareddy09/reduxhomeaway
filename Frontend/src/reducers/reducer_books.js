import _ from "lodash";
import {  SIGNUP_CHECK, CREATE_LOCATION, SEARCH_PROPERTY, CREATE_PROFILE, POST_PROPERTY, LOGIN_CHECK } from "../actions";


//Reducer listening to different action types
//initial state is {}
export default function(state = {}, action) {
  console.log("i am in reducer signup");
  if(action.payload){
  console.log("action,",action.payload.data);
  }
  console.log("signupdetails",state);
  switch (action.type) {
    case SIGNUP_CHECK:
    return {
      ...state,
      signupdetails: action.payload.data
    }
    case CREATE_LOCATION:
    return {
      ...state,
      form: action.payload
    }
    case POST_PROPERTY:
    return {
      ...state,
      propertydetails: action.payload
    }
    case SEARCH_PROPERTY:
    return {
      ...state,
      searchdetails: action.payload
    }
    case CREATE_PROFILE:
    return {
      ...state,
      profiledetails: action.payload
    }
    case LOGIN_CHECK:
    return {
      ...state,
      loginstatus: action.payload
    }
    default:
      return state;
  }
}
