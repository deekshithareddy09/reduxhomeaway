import axios from "axios";

export const SIGNUP_CHECK = "signupCheck";
export const LOGIN_CHECK = "logincheck";
export const PROFILE = "profile";
export const CREATE_LOCATION = "createLocation";
export const SEARCH_PROPERTY = "searchProperty";
export const CREATE_PROFILE = "profile";
export const POST_PROPERTY = "postProperty";

const ROOT_URL = "";


export function profile(profiledata) {
  //middleware call
  //receive response from backend
  console.log("action",profiledata);
  console.log("i am in console action");
  const response = axios.post(`${ROOT_URL}/profile`,profiledata)
            .then((response) => {
                console.log("Status Code : ",response.status);
                if(response.status === 200){
                    console.log("response data ",response);
                }
            });
            return {
                type: CREATE_PROFILE,
                payload: response
              }; 

}




export function createLocation(values, callback) {
    
    console.log("in createLocation",values);
    const request = axios
    .post(`${ROOT_URL}/propertyidstore`, values);
    callback();

  return {
    type: SIGNUP_CHECK,
    payload: values
  };
}
// export function logincheck(logindata, callback) {
//   axios.defaults.withCredentials = true;
//         //make a post request with the user data
//         const request = axios.post(`${ROOT_URL}/logincheck`,logindata)
//             .then((response) => {
//                 console.log("Status Code : ",response.status);
//                 if(response.status === 200){
//                     console.log("response data ");
//                     callback();                   
//                 }
//             });
//           }
// signup check


export function logincheck(logindata, callback) {
    axios.defaults.withCredentials = true;
    console.log("i am in login in actions");
    //make a post request with the user data
    const response = axios.post(`${ROOT_URL}/logincheck`,logindata).then((data)=>{
      callback(data);
    })
        return {
          type: LOGIN_CHECK,
          payload: response
        }
      }




export function signupCheck(signupdata, callback) {
axios.defaults.withCredentials = true;
console.log("i am in signupcheck in actions");
//make a post request with the user data
const response = axios.post(`${ROOT_URL}/signupcheck`,signupdata);
    console.log("response",response.value);
     callback();
    return {
      type: SIGNUP_CHECK,
      payload: response
    }
  }


  export function postProperty(values, callback) {
     //set the with credentials to true
     axios.defaults.withCredentials = true;
     //make a post request with the user data
     const response = axios.post(`${ROOT_URL}/postProperty`,values)
     .then(response => {
             console.log("Status Code : ",response.status);
             if(response.status === 200){
                callback();
            }else{
                 console.log("error in getting the response ");
             }
         });
         return {
            type: POST_PROPERTY,
            payload: response
          } 
      }
    
 export function searchProperty(data, callback) {
       //set the with credentials to true
       axios.defaults.withCredentials = true;
       //make a post request with the user data
       const response = axios.post('/dashboardsearch',data)
           console.log("reponse data from dashboard",response);
           callback();
           return {
            type: SEARCH_PROPERTY,
            payload: response
          }
           
        }
      