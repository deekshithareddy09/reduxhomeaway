'use strict';
var mongoose = require('mongoose');

var myuri='mongodb://localhost:27017/homeaway';

    mongoose.Promise = global.Promise;

    mongoose.createConnection('mongodb://localhost:27017/homeaway', {poolSize: 4 });



    // mongoose.connect('mongodb://localhost:27017/homeaway');

    mongoose.connect(myuri, {
        socketTimeoutMS: 0,
        keepAlive: true,
        reconnectTries: 30
      });

    module.exports = {mongoose,
        'secret': "Passphrase for encryption should be 45-50 char long"
    };