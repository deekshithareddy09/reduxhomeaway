//import the require dependencies
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
var mysql = require('mysql');
var session = require('express-session');
var cookieParser = require('cookie-parser');
// var {mongoose} = require('./db/mongoose');
var {Users} = require('./models/user');
var {Profile} = require('./models/profile');
var {Booking} = require('./models/booking');
var {Photo} = require('./models/photo');
var {Propertydetails} = require('./models/propertydetails');
var {Travelhistory} = require('./models/travelhistory');
const multer = require('multer');
const uuidv4 = require('uuid/v4');
const path = require('path');
const fs = require('fs');
var propertyid="";
var passport = require('passport');
app.set('port', process.env.PORT || 3001);
var config = require('./config/settings');
var jwt = require('jsonwebtoken');
var crypt = require('./app/crypt');
var db = require('./app/db');
var requireAuth = passport.authenticate('jwt', {session: false});
    const storage = multer.diskStorage({
      destination: (req, file, cb) => {
    var dir="./" +propertyid;
    console.log(dir);
    if (!fs.existsSync(dir)) 
       fs .mkdirSync(dir); 
       cb(null,dir);
      },
      filename: (req, file, cb) => {
        const newFilename = file.originalname;
        console.log("new file name is"+newFilename);

        var photo = new Photo({
        propertyid : propertyid,
        filename :  newFilename
        });
        photo.save().then((photoname)=>{
        console.log("Photo details posted successfully: ",photo);
        },(err)=>{
        console.log("Error Creating photo");
        });
        cb(null, newFilename);
      },
    });
    
    const upload = multer({ storage });


//use cors to allow cross origin resource sharing
app.use(cors({ origin: '*', credentials: true }));
// Serve any static files
  app.use(express.static(path.join(__dirname, '../Frontend/build')));
  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '../Frontend/build', 'index.html'));
  });


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));



//sesssion
app.use(session({
    secret              : 'cmpe273_kafka_passport_mongo',
    resave              : false, // Forces the session to be saved back to the session store, even if the session was never modified during the request
    saveUninitialized   : false, // Force to save uninitialized session to db. A session is uninitialized when it is new but not modified.
    duration            : 60 * 60 * 1000,    // Overall duration of Session : 30 minutes : 1800 seconds
    activeDuration      :  5 * 60 * 1000
}));


app.use(bodyParser.json());

app.use(passport.initialize());
require('./config/passport')(passport);

//Allow Access Control
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    res.setHeader('Cache-Control', 'no-cache');
    next();
  });

  app.post('/signupcheck', function (request, response) {
    console.log(request.body);
    if (!request.body.username || !request.body.password) {
        response.status(400).json({success: false, message: 'Please enter username and password.'});
    } else {
        var newUser = {
            username: request.body.username,
            password: request.body.password,
            firstname: request.body.firstname,
            lastname: request.body.lastname,
            type: request.body.type
        };

        // Attempt to save the user
        db.createUser(newUser, function (res) {
            response.writeHead(200,{
                          'Content-Type' : 'application/json'
                         });
                         response.end(JSON.stringify(newUser));
        }, function (err) {
            console.log(err);
            return response.status(400).json({success: false, message: 'That username address already exists.'});
        });
    }
});




// property set

app.post('/propertyidstore',function(req,res){
    console.log("Inside propertyid storing ");
    console.log(req.body);
    propertyid= req.body.propid;
    console.log("propertyid",propertyid);
});



// code for booking

app.post('/booking',  function(req,res){
    console.log("Inside propertyid storing ");
    console.log(req.body);
    console.log("propertyid",propertyid);
    var booking = new Booking({
        username : req.body.username,
        propid : req.body.propid
    });
    Propertydetails.findOneAndUpdate({propid: req.body.propid}, {$set:{travelerbooked: req.body.username}}, {new: true}, (err, doc) => {
        if (err) {
            console.log("Something wrong when updating data!");
        }
        console.log("booking history saved in owner successfully",doc);    
    });
    booking.save().then((booking)=>{
        console.log("Booking details posted successfully: ",booking);
                res.writeHead(200,{
            'Content-Type' : 'application/json'
        });
        res.end("Booking successfully posted");
    },(err)=>{
        console.log("Error Creating Book");
        res.sendStatus(400).end();
    })  
});

app.post('/logincheck', function (request, response) {
    db.findUser({
        username: request.body.username
    }, function (res) {
        console.log("res"+res);
        var user = {
            id: res.id,
            username: res.username,
        };
        console.log("i n he success callback");
        // Check if password matches
        crypt.compareHash(request.body.password, res.password, function (err, isMatch) {
            if (isMatch && !err) {
                // Create token if the password matched and no error was thrown
                var token = jwt.sign(user, config.secret, {
                    expiresIn: 10080 // in seconds
                });
                console.log('The response is successful');
                console.log("Login successful after comparing hashes",isMatch);
                response.status(200).json({success: true, token: 'JWT ' + token});
            } else {
                console.log("The response is 401");
                response.status(401).json({
                    success: false,
                    message: 'Authentication failed. Passwords did not match.'
                });
            }
        }, function (err) {
            console.log(err);
            response.status(401).json({success: false, message: 'Authentication failed. User not found.'});
        });
    }, function (err) {
        console.log(err);
        response.status(401).json({success: false, message: 'Authentication failed. User not found.'});
    });
});



app.post('/postProperty', function(req,res){
    
    var property = new Propertydetails({
        username : req.body.username,
        propid : req.body.propid,
        address : req.body.address,
        city : req.body.city,
        state : req.body.state,
        headline : req.body.headline,
        description : req.body.description,
        propertytype : req.body.propertytype,
        bedrooms : req.body.bedrooms,
        bathrooms : req.body.bathrooms,
        accomodates : req.body.accomodates,
        instantbooking : req.body.instantbooking,
        currency : req.body.currency,
        nightbasedrate : req.body.nightbasedrate,
        stay : req.body.stay,
        startdate : req.body.startdate,
        enddate : req.body.enddate,
        travelerbooked : "not booked "
    });
    property.save().then((property)=>{
        console.log("Property details posted successfully: ");
        res.writeHead(200,{
            'Content-Type' : 'application/json'
        });
        res.end("property successfully posted");
    },(err)=>{
        console.log("Error Creating Book");
        res.sendStatus(400).end();
    })    
    
})






app.post('/profile',async (req,res)=>{
    try{
    console.log(req.body.firstname);
    console.log("i am in profile");
    const profiledetails = await Profile.findOneAndUpdate(
            {username: req.body.username}, 
            {$set:{firstname: req.body.firstname,
                   lastname: req.body.lastname,
                   aboutme: req.body.aboutme,
                   city: req.body.city,
                   company: req.body.company,
                   school: req.body.school,
                   hometown: req.body.hometown,
                   languages: req.body.languages,
                   gender: req.body.gender,
                   phone: req.body.phone
            }},
            {new : true}
            );
            const user = await Users.findOneAndUpdate(
                {username: req.body.username}, 
                {$set:{firstname: req.body.firstname,
                       lastname: req.body.lastname,
                      }
                },
                {new : true}
            );
            console.log("Profile details entered successfully");
            res.sendStatus(200).end();
            } catch(err){
            res.sendStatus(400).end();
            console.log("Error occured in the profile update");
            }
        }
    );
    
//Route for photo uploading
app.post('/photoupload', upload.array('newArray','newArray.size'), (req, res) => {
    //console.log("selectedfile"+selectedFile);
    //console.log("Res : ",res.file);
    res.send();
});

//route for travleer data reteieval in owner dashboard
app.post('/photoname', async (req,res) =>{
    var photoid = req.body.photoid;
    console.log("photoidin photoname is ",photoid);
    await Photo.find({
        propertyid:req.body.photoid
    }, function(err,photo){
        if (err) {
            res.code = "400";
            console.log(res.value);
            res.sendStatus(400).end(); 
        } else {
            console.log("The photo retrieval",photo);
            res.writeHead(200,{
                'Content-Type' : 'application/json'
            });
            res.end(JSON.stringify(photo));
        }
    })
})



//route for travleer data reteieval in owner dashboard
app.post('/travelerbooking', (req,res) =>{
    var username = req.body.travelerbooked;
    console.log("Username in travelerbooking:",username);
    Users.findOne({
        username:req.body.travelerbooked
    }, function(err,user){
        if (err) {
            res.code = "400";
            res.value = "The email and password you entered did not match our records. Please double-check and try again.";
            console.log(res.value);
            res.sendStatus(400).end(); 
        } else {
            console.log("traveler data retireving",user);
            res.writeHead(200,{
                'Content-Type' : 'application/json'
            });
            res.end(JSON.stringify(user));
        }
    })
})



app.post('/ownerdashboard', (req,res) =>{
    console.log("I am in ownerdashboard");
    var username = req.body.username;
    console.log("Username:",username );
    Propertydetails.find({
        username:req.body.username
    }, function(err,propertydetails){
        if (err) {
            res.code = "400";
            res.value = "The email and password you entered did not match our records. Please double-check and try again.";
            console.log(res.value);
            res.sendStatus(400).end(); 
        } else {
            res.writeHead(200,{
                'Content-Type' : 'application/json'
            });
            res.end(JSON.stringify(propertydetails));
        }
    })
})


// route for owner property photo retirvel
app.post('/download/:file(*)',(req, res) => {
    console.log("Inside download file",req);
    var file = req.params.file;
    console.log("file is"+file);
    var photoid = req.body.photoid;
    console.log(photoid);
    var fileLocation = path.join("./" +photoid,file);
    console.log("file location is"+fileLocation);
    var img = fs.readFileSync(fileLocation);
    var base64img = new Buffer(img).toString('base64');
    res.writeHead(200, {'Content-Type': 'image/jpg' });
    res.end(base64img);
  });

  //Route for dashboard search
  app.post('/dashboardsearch',function(req,res){
      
      console.log("Inside Dashboard Request");
     
       //con.connect(function(err) {
           //if (err) 
            //throw err;
         console.log("Connected!");
         console.log(req.body);
          var location = req.body.location;
        //   var startDate = req.body.startDate.substring(0, 10);
        //   var endDate = req.body.endDate.substring(0, 10);
          var startDate = req.body.startDate;
          var endDate = req.body.endDate;
          console.log(startDate+endDate);
          var username = req.body.username;
          Propertydetails.find({"enddate":{$gte:endDate},
                                "startdate":{$lte:startDate},
                                "city":location  
                            }, function(err,propertydetails){
                                if (err) {
                                    res.code = "400";
                                    res.value = "The email and password you entered did not match our records. Please double-check and try again.";
                                    console.log(res.value);
                                    res.sendStatus(400).end(); 
                                } else {
                                    console.log("property",propertydetails);
                                    console.log("I am in travelr dash");
                                    console.log("propertyid",propertydetails);
                                    propertydetails.map(propertydetailsitem => {
                                        var travelerhistorydetails = new Travelhistory({
                                            username : req.body.username,
                                            propid : propertydetailsitem.propid
                                        });
                                        travelerhistorydetails.save().then((travelerhistorydetails)=>{
                                            console.log("travler history details entered successfully while dashboard serach: ",travelerhistorydetails);
                                        }, (err)=>{
                                            console.log("Error while entering details");
                                        })
                                    });
                                    res.writeHead(200,{
                                        'Content-Type' : 'application/json'
                                    });
                                    res.end(JSON.stringify(propertydetails));
                                }
                            });
  });

  

  app.post('/tripboardafterclick', (req,res) =>{
    console.log("I am in tripbardclick");
    var username = req.body.username;
    console.log("Username:",username );
    Travelhistory.find({
        username:req.body.username
    }, function(err,travelhistory){
        if (err) {
            res.code = "400";
            res.value = "The email and password you entered did not match our records. Please double-check and try again.";
            console.log(res.value);
            res.sendStatus(400).end(); 
        } else {
            res.writeHead(200,{
                'Content-Type' : 'application/json'
            });
            res.end(JSON.stringify(travelhistory));
        }
    })
});



//start your server on port 3001
app.listen(app.get('port'), function(){
    console.log(("Express server listening on port " + app.get('port')))
  });


// fs.readdir(testFolder, (err, files) => {
//     files.forEach(file => {
//       // var f = file.substr(0, file.lastIndexOf('.'));
//       ImgNames.push(file);
//       console.log("filename is  "+file);
//       console.log("img names array length is"+ImgNames.length)
  
//     });
//   })

