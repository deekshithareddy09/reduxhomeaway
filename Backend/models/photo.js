var mongoose = require('mongoose');

var Photo = mongoose.model('Photo',{
    propertyid :{
        type : String
    },
    filename :{
        type : String
    }
});

module.exports = {Photo};