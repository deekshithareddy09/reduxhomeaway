var mongoose = require('mongoose');

var Propertydetails = mongoose.model('Propertydetails',{
    username :{
        type : String
    },
    address :{
        type : String
    },
    city :{
        type : String
    },
    state :{
        type : String
    },
    propid :{
        type : String
    },
    headline :{
        type : String
    },
    description :{
        type : String
    },
    propertytype :{
        type : String
    },
    bedrooms :{
        type : String
    },
    bathrooms :{
        type : String
    },
    accomodates :{
        type : String
    },
    instantbooking : {
        type : String
    },
    currency :{
        type : String
    },
    nightbasedrate :{
        type : String
    },
    stay :{
        type : String
    },
    startdate : {
        type : Date
    },
    enddate :{
        type : Date
    }, 
    travelerbooked :{
        type : String
    }
});

module.exports = {Propertydetails};
