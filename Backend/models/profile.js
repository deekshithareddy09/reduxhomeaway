var mongoose = require('mongoose');

var Profile = mongoose.model('Profile',{
    firstname :{
        type : String
    },
    lastname :{
        type : String
    },
    username :{
        type : String
    },
    aboutme :{
        type : String
    },
    city :{
        type : String
    },
    company :{
        type : String
    },
    school :{
        type : String
    },
    hometown :{
        type : String
    },
    languages :{
        type : String
    },
    gender :{
        type : String
    },
    phone :{
        type : String
    },
    type : {
        type : String
    }
});

module.exports = {Profile};
