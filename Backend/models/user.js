
// var mongoose = require('mongoose'),
//     Schema = mongoose.Schema,
//     passportLocalMongoose = require('passport-local-mongoose');

// var UserSchema = new Schema({
//     username: String,
//     password: String,
//     firstname: String,
//     lastname: String,
//     type: String
// });

// Users.plugin(passportLocalMongoose);

// // module.exports = mongoose.model('Users', Users);
// module.exports =  mongoose.model('Users', UserSchema)
// var mongoose = require('mongoose');
var mongoose = require('mongoose');


var Users = mongoose.model('Users',{
    firstname :{
        type : String
    },
    lastname :{
        type : String
    },
    username :{
        type : String
    },
    password : {
        type : String
    },
    type : {
        type : String
    }
});

module.exports = {Users};