var express = require('express');
var router = express.Router();
var db = require('../app/db');

router.post('/', function (request, response) {
    if (!request.body.username || !request.body.password) {
        response.status(400).json({success: false, message: 'Please enter username and password.'});
    } else {
        var newUser = {
            username: request.body.username,
            password: request.body.password,
            firstname: request.body.firstname,
            lastname: request.body.lastname,
            type: request.body.type
        };

        // Attempt to save the user
        db.createUser(newUser, function (res) {
            response.writeHead(200,{
                          'Content-Type' : 'application/json'
                         });
                         response.end(JSON.stringify(newUser));
        }, function (err) {
            console.log(err);
            return response.status(400).json({success: false, message: 'That username address already exists.'});
        });
    }
});
 module.exports = router;