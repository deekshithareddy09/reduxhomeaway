var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config/settings');
var jwt = require('jsonwebtoken');
var crypt = require('../app/crypt');
var db = require('../app/db');
var requireAuth = passport.authenticate('jwt', {session: false});






router.post('/logincheck', function (request, response) {
    console.log("I am in the logincheck");
    db.findUser({
        username: request.body.username
    }, function (res) {
        console.log("res"+res);
        var user = {
            id: res.id,
            username: res.username,
        };
        console.log("i n he success callback");
        // Check if password matches
        crypt.compareHash(request.body.password, res.password, function (err, isMatch) {
            if (isMatch && !err) {
                // Create token if the password matched and no error was thrown
                var token = jwt.sign(user, config.secret, {
                    expiresIn: 10080 // in seconds
                });
                console.log("Login successful after comparing hashes",isMatch);
                response.status(200).json({success: true, token: 'JWT ' + token});
            } else {
                console.log("The response is 401");
                response.status(401).json({
                    success: false,
                    message: 'Authentication failed. Passwords did not match.'
                });
            }
        }, function (err) {
            console.log(err);
            response.status(401).json({success: false, message: 'Authentication failed. User not found.'});
        });
    }, function (err) {
        console.log(err);
        response.status(401).json({success: false, message: 'Authentication failed. User not found.'});
    });
});

module.exports = router;
