'use strict';
var crypt = require('./crypt');
var config = require('../config/settings');
var {Users} = require('../models/user');
var {Profile} = require('../models/profile');
var mongoose = require('mongoose');
var db = {};
db.createUser = function (user, successCallback, failureCallback) {
    var passwordHash;
    crypt.createHash(user.password, function (res) {
        passwordHash = res;
        console.log("passwordjhash",passwordHash);
        var usertest = new Users({
            firstname : user.firstname,
            lastname : user.lastname,
            username : user.username,
            password : passwordHash,
            type : user.type
        });
        var profiledetails = new Profile({
            firstname : user.firstname,
            lastname : user.lastname,
            username : user.username,
            type : user.type
        });
        usertest.save().then((usertest)=>{
            console.log("User Signup Successful : ",usertest);
            profiledetails.save().then((profiledetails)=>{
                console.log("profile details entered successfully while signing up : ",profiledetails);
                successCallback();
            }, (err)=>{
             failureCallback(err);
                console.log("Error Creating Book");
            })
        },(err)=>{
            console.log("Error Creating Book");
            failureCallback();
        })      
    }, function (err) {
        console.log(err);
        failureCallback();
    });
};

db.findUser = function (user, successCallback, failureCallback) {
    var username = user.username;
    console.log("Username:",username );
    Users.findOne({
        username:user.username
    }, function(err,usersaved){
        console.log("usersaved",usersaved);
        if (err) {
            failureCallback();
        }
        else if(usersaved == null) {
            failureCallback();
        }else {
            console.log("Successfully logged in");
            successCallback(usersaved);
        }
    })
};

module.exports = db;